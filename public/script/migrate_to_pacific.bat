set DB=db:pg://%USER%:%PGPASSWORD%@%HOST%:%PORT%/postgres
set PGPASSWORD=
set HOST=
set PORT=
set DB_NAME=postgres
set USER=
set CHANGE=

pg_dump -h %HOST% -p %PORT% -d %DB_NAME% -U %USER% -N hdb_catalog -N hdb_views -N sqitch -a > pg_dump_test_data.sql

psql -h %HOST% -p %PORT% -d %DB_NAME% -U %USER% -f delete_schemas.sql

cd ..
call cmd /c sqitch deploy %DB% --to-change %CHANGE%

cd script
psql -h %HOST% -p %PORT% -d %DB_NAME% -U %USER% -f pg_dump_test_data.sql

call delete_stateless.bat
cd ../..
call cmd /c sqitch deploy %DB%
cd script
call create_stateless.bat
del pg_dump_test_data.sql
cd ..