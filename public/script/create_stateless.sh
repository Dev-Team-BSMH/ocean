export PGPASSWORD=developmenteam
cd create_stateless
psql -h $HOST -p $PORT -d $DB_NAME -U $USER  -f create_core_stateless.sql
psql -h $HOST -p $PORT -d $DB_NAME -U $USER  -f create_eval_stateless.sql
psql -h $HOST -p $PORT -d $DB_NAME -U $USER  -f create_records_stateless.sql
psql -h $HOST -p $PORT -d $DB_NAME -U $USER  -f create_mantle_stateless.sql
psql -h $HOST -p $PORT -d $DB_NAME -U $USER  -f create_api_all_stateless.sql
psql -h $HOST -p $PORT -d $DB_NAME -U $USER  -f create_api_now_stateless.sql