SET
  CLIENT_ENCODING TO 'utf8';
---- core ----
  --
  -- Name: active_roles; Type: VIEW; Schema: core
  --
  CREATE OR REPLACE VIEW core.active_roles AS
  SELECT
    roles.role,
    users.soldier_id,
    users_roles.id AS user_role_id
  FROM (
  (
    core.users_roles
    JOIN core.roles ON ((roles.id = users_roles.role_id))
  )
  JOIN core.users ON ((users.id = users_roles.user_id))
  )
  WHERE (
    NOT (
      users_roles.id IN (
      SELECT
        users_roles_termination.user_role_id
      FROM core.users_roles_termination
      )
    )
  );
--
  -- Name: course_commanders; Type: VIEW; Schema: core
  --
  CREATE OR REPLACE VIEW core.course_commanders AS
SELECT
  users_roles.id,
  users_roles.user_id
FROM (
    core.users_roles
    JOIN core.roles ON (
        (
          (roles.id = users_roles.role_id)
          AND (roles.role = 'מק"ס"' :: text)
        )
      )
  );
--
  -- Name: students; Type: VIEW; Schema: core
  --
  CREATE OR REPLACE VIEW core.students AS
SELECT
  users_roles.id,
  users_roles.user_id
FROM (
    core.users_roles
    JOIN core.roles ON (
        (
          (roles.id = users_roles.role_id)
          AND (roles.role = 'חניך' :: text)
        )
      )
  );
--
  -- Name: users_roles_groups; Type: VIEW; Schema: core
  --
  CREATE OR REPLACE VIEW core.users_roles_groups AS
SELECT
  users_roles.id AS user_role_id,
  assigns.group_id
FROM (
    core.users_roles
    JOIN core.assigns ON ((assigns.user_role_id = users_roles.id))
  )
ORDER BY
  users_roles.id,
  assigns.group_id;
--
  -- Name: cycles_students; Type: VIEW; Schema: core
  --
  CREATE OR REPLACE VIEW core.cycles_students AS WITH RECURSIVE users_roles_cycles(cycle_id, group_child_id) AS (
    SELECT
      groups.id AS cycle_id,
      groups.id AS group_child_id
    FROM (
        core.groups
        JOIN core.group_types ON ((group_types.id = groups.group_type_id))
      )
    WHERE
      (group_types.type = 'מחזור' :: text)
    UNION
    SELECT
      users_roles_cycles_1.cycle_id,
      edges_1.child AS group_child_id
    FROM (
        users_roles_cycles users_roles_cycles_1
        JOIN core.edges edges_1 ON (
            (
              edges_1.parent = users_roles_cycles_1.group_child_id
            )
          )
      )
  )
SELECT
  DISTINCT users_roles_cycles.cycle_id,
  users_roles_groups.user_role_id AS student_id
FROM (
    (
      users_roles_cycles
      JOIN core.users_roles_groups ON (
          (
            users_roles_groups.group_id = users_roles_cycles.group_child_id
          )
        )
    )
    JOIN core.students ON ((students.id = users_roles_groups.user_role_id))
  )
ORDER BY
  users_roles_cycles.cycle_id,
  users_roles_groups.user_role_id;
--
  -- Name: department_commanders; Type: VIEW; Schema: core
  --
  CREATE OR REPLACE VIEW core.department_commanders AS
SELECT
  users_roles.id,
  users_roles.user_id
FROM (
    core.users_roles
    JOIN core.roles ON (
        (
          (roles.id = users_roles.role_id)
          AND (roles.role = 'מ"מ"' :: text)
        )
      )
  );
--
  -- Name: group_ancestors; Type: VIEW; Schema: core
  --
  CREATE OR REPLACE VIEW core.group_ancestors AS WITH RECURSIVE group_ancestors(group_id, ancestor_id) AS (
    SELECT
      groups.id AS group_id,
      edges.parent AS ancestor_id,
      1 AS depth
    FROM (
        core.groups
        JOIN core.edges ON ((edges.child = groups.id))
      )
    UNION
    SELECT
      group_ancestors_1.group_id,
      edges.parent AS ancestor_id,
      (group_ancestors_1.depth + 1) AS depth
    FROM (
        (
          core.groups
          JOIN group_ancestors group_ancestors_1 ON ((group_ancestors_1.group_id = groups.id))
        )
        JOIN core.edges ON ((edges.child = group_ancestors_1.ancestor_id))
      )
  )
SELECT
  group_ancestors.group_id,
  group_ancestors.ancestor_id,
  group_ancestors.depth
FROM group_ancestors
UNION
SELECT
  groups.id AS group_id,
  groups.id AS ancestor_id,
  0 AS depth
FROM core.groups
ORDER BY
  1,
  2;
--
  -- Name: group_for_soldier_id; Type: VIEW; Schema: core
  --
  CREATE OR REPLACE VIEW core.group_for_soldier_id AS
SELECT
  users.soldier_id,
  group_ancestors.ancestor_id AS group_id
FROM (
    (
      (
        core.users_roles
        JOIN core.assigns ON ((assigns.user_role_id = users_roles.id))
      )
      JOIN core.users ON ((users_roles.user_id = users.id))
    )
    JOIN core.group_ancestors ON ((group_ancestors.group_id = assigns.group_id))
  )
ORDER BY
  users.soldier_id,
  group_ancestors.ancestor_id;
--
  -- Name: instructors; Type: VIEW; Schema: core
  --
  CREATE OR REPLACE VIEW core.instructors AS
SELECT
  users_roles.id,
  users_roles.user_id
FROM (
    core.users_roles
    JOIN core.roles ON (
        (
          (roles.id = users_roles.role_id)
          AND (roles.role = 'מדריך' :: text)
        )
      )
  );
--
  -- Name: zapa; Type: VIEW; Schema: core
  --
  CREATE OR REPLACE VIEW core.zapa AS
SELECT
  users_roles.id,
  users_roles.user_id
FROM (
    core.users_roles
    JOIN core.roles ON (
        (
          (roles.id = users_roles.role_id)
          AND (roles.role = 'צפ"ה"' :: text)
        )
      )
  );

   CREATE OR REPLACE VIEW core.cycles_for_user_id AS (
     SELECT 
      soldier_groups.user_id,
      soldier_groups.group_id,
      soldier_groups.user_role_id
     FROM (
      SELECT DISTINCT 
        users.id AS user_id,
        group_ancestors.ancestor_id AS group_id,
        users_roles.id as user_role_id
      FROM core.users
        JOIN core.users_roles ON users_roles.user_id = users.id
        JOIN core.assigns ON assigns.user_role_id = users_roles.id
        JOIN core.group_ancestors ON group_ancestors.group_id = assigns.group_id
        JOIN core.groups ON groups.id = group_ancestors.ancestor_id
      UNION
      SELECT DISTINCT 
        users.id AS user_id,
        group_ancestors.group_id AS group_id,
        users_roles.id as user_role_id
      FROM core.users
        JOIN core.users_roles ON users_roles.user_id = users.id
        JOIN core.assigns ON assigns.user_role_id = users_roles.id
        JOIN core.group_ancestors ON group_ancestors.ancestor_id = assigns.group_id
        JOIN core.groups ON groups.id = group_ancestors.group_id) soldier_groups
      JOIN core.groups ON groups.id = soldier_groups.group_id
      JOIN core.group_types ON group_types.id = groups.group_type_id
      WHERE group_types.type = 'מחזור'
      ORDER BY 1,2,3);
  
CREATE OR REPLACE VIEW core.users_for_group
 AS
 SELECT DISTINCT group_ancestors.ancestor_id AS group_id,
     	users.id AS user_id
	FROM core.group_ancestors
	JOIN core.assigns ON assigns.group_id = group_ancestors.group_id
	JOIN core.users_roles ON users_roles.id = assigns.user_role_id
	JOIN core.users ON users.id = users_roles.user_id
  ORDER BY group_ancestors.ancestor_id, users.id
  ;
--
  -- Name: check_if_student(integer); Type: FUNCTION; Schema: public; Owner: devteam
  --
  CREATE FUNCTION core.check_if_student(student_id integer) RETURNS boolean LANGUAGE sql AS $$
SELECT
  EXISTS (
    SELECT
      students.id
    FROM core.students
    WHERE
      students.id = student_id
  ) $$;