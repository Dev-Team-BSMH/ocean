SET
  CLIENT_ENCODING TO 'utf8';

CREATE FUNCTION mantle.check_if_course(course_id integer) RETURNS boolean LANGUAGE sql AS $$
SELECT
  EXISTS (
    SELECT
      groups.id,
      groups.name,
      group_types.type
    FROM core.groups
    INNER JOIN core.group_types ON groups.group_type_id = group_types.id
    WHERE
      group_types.type = 'קורס'
      AND groups.id = course_id
  ) $$;

ALTER TABLE mantle.course_shortcuts
ADD
  CONSTRAINT check_if_course CHECK (mantle.check_if_course(course_id)); 

CREATE FUNCTION mantle.check_if_mador(mador_id integer) RETURNS boolean LANGUAGE sql AS $$
SELECT
  EXISTS (
    SELECT
      groups.id,
      groups.name,
      group_types.type
    FROM core.groups
    INNER JOIN core.group_types ON groups.group_type_id = group_types.id
    WHERE
      group_types.type = 'מדור'
      AND groups.id = mador_id
  ) $$;

ALTER TABLE mantle.mador_shortcuts
ADD
  CONSTRAINT check_if_mador CHECK (mantle.check_if_mador(mador_id));
