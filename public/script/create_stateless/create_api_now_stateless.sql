SET CLIENT_ENCODING TO 'utf8';
---- api_now ----
DROP SCHEMA IF EXISTS api_now cascade;
CREATE SCHEMA IF NOT EXISTS api_now;

--@ CORE @--
-- Groups (All)
CREATE OR REPLACE VIEW api_now.groups AS
SELECT *
FROM api_all.groups
WHERE (start_date is null AND end_date is null) OR (start_date <= now() AND now() <= end_date);

-- Madors
CREATE OR REPLACE VIEW api_now.madors AS
SELECT *
FROM api_all.madors;

-- Megamot
CREATE OR REPLACE VIEW api_now.megamot AS
SELECT *
FROM api_all.megamot;

-- Cycles
CREATE OR REPLACE VIEW api_now.cycles AS
SELECT cycles.*
FROM api_all.cycles
JOIN api_now.groups ON groups.id = cycles.id;

-- Courses
CREATE OR REPLACE VIEW api_now.courses AS
SELECT courses.*
FROM api_all.courses
JOIN api_now.groups ON groups.id = courses.id;

-- Departments
CREATE OR REPLACE VIEW api_now.departments AS
SELECT departments.*
FROM api_all.departments
JOIN api_now.courses ON departments.course_id = courses.id;

-- Teams
CREATE OR REPLACE VIEW api_now.teams AS
SELECT teams.*
FROM api_all.teams
JOIN api_now.courses ON teams.course_id = courses.id;

-- Departments and Teams
CREATE OR REPLACE VIEW api_now.departments_and_teams AS
SELECT departments_and_teams.*
FROM api_all.departments_and_teams
JOIN api_now.courses ON departments_and_teams.course_id = courses.id;

-- Assignments (referred from the user views via user_id)
CREATE OR REPLACE VIEW api_now.assignments AS
SELECT assignments.*
FROM api_all.assignments
JOIN api_now.groups ON groups.id = assignments.group_id;

-- Students
CREATE OR REPLACE VIEW api_now.students AS
SELECT students.*
FROM api_all.students
JOIN api_now.assignments ON assignments.user_id = students.user_id;

-- Instructors
CREATE OR REPLACE VIEW api_now.instructors AS
SELECT instructors.*
FROM api_all.instructors
JOIN api_now.assignments ON assignments.user_id = instructors.user_id;

-- Makasim
CREATE OR REPLACE VIEW api_now.makasim AS
SELECT makasim.*
FROM api_all.makasim
JOIN api_now.assignments ON assignments.user_id = makasim.user_id;

-- Tzapaot
CREATE OR REPLACE VIEW api_now.tzapaot AS
SELECT tzapaot.*
FROM api_all.tzapaot
JOIN api_now.assignments ON assignments.user_id = tzapaot.user_id;

-- Users
CREATE OR REPLACE VIEW api_now.users AS
SELECT users.*
FROM api_all.users
JOIN api_now.assignments ON assignments.user_id = users.user_id;

--@ EVAL @--
-- All
CREATE OR REPLACE VIEW api_now.milestones AS 
SELECT milestones.*
FROM api_all.milestones
JOIN eval.group_eval_roots ON group_eval_roots.root_id=milestones.root_id
JOIN api_now.courses ON courses.id=group_eval_roots.group_id;

-- Leaves
CREATE OR REPLACE VIEW api_now.leaves AS 
SELECT leaves.*
FROM api_all.leaves
JOIN eval.group_eval_roots ON group_eval_roots.root_id=leaves.root_id
JOIN api_now.courses ON courses.id=group_eval_roots.group_id;

--- Milestone types: ---
-- criterions
CREATE OR REPLACE VIEW api_now.criterions AS 
SELECT criterions.*
FROM api_all.criterions
JOIN eval.group_eval_roots ON group_eval_roots.root_id=criterions.root_id
JOIN api_now.courses ON courses.id=group_eval_roots.group_id;
-- meetings
CREATE OR REPLACE VIEW api_now.meetings AS 
SELECT meetings.*
FROM api_all.meetings
JOIN eval.group_eval_roots ON group_eval_roots.root_id=meetings.root_id
JOIN api_now.courses ON courses.id=group_eval_roots.group_id;
-- tests: מבחן, מבחן חוזר, תר"ץ, מדרגה
CREATE OR REPLACE VIEW api_now.tests AS 
SELECT tests.*
FROM api_all.tests
JOIN eval.group_eval_roots ON group_eval_roots.root_id=tests.root_id
JOIN api_now.courses ON courses.id=group_eval_roots.group_id;
-- professions
CREATE OR REPLACE VIEW api_now.professions AS 
SELECT professions.*
FROM api_all.professions
JOIN eval.group_eval_roots ON group_eval_roots.root_id=professions.root_id
JOIN api_now.courses ON courses.id=group_eval_roots.group_id;
-- Tree roots
CREATE OR REPLACE VIEW api_now.tree_roots AS 
SELECT tree_roots.*
FROM api_all.tree_roots
JOIN eval.group_eval_roots ON group_eval_roots.root_id=tree_roots.id
JOIN api_now.courses ON courses.id=group_eval_roots.group_id;

--- Grades ---
CREATE OR REPLACE VIEW api_now.grades AS
SELECT grades.*
FROM api_all.grades
JOIN api_now.students ON students.user_id = grades.student_id