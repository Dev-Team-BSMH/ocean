SET
  CLIENT_ENCODING TO 'utf8';
----records----
  --
  -- Name: public_posts; Type: VIEW; Schema: records; Owner: devteam
  --
CREATE OR REPLACE VIEW records.public_posts AS
  SELECT posts.id AS post_id
  FROM records.posts
  WHERE NOT (posts.id IN ( SELECT DISTINCT post_visibility_for_users.post_id
    FROM records.post_visibility_for_users
    JOIN core.users_roles ON post_visibility_for_users.user_role_id = users_roles.id
    JOIN core.roles ON roles.id = users_roles.role_id AND roles.role <> 'חניך'::text));

CREATE OR REPLACE FUNCTION records.check_if_exit_req(post_id integer) 
  RETURNS boolean  AS $$
    SELECT EXISTS(
    SELECT * FROM records.posts
    JOIN records.post_types ON post_types.id = posts.type_id
    WHERE post_types."type" = 'בקשת יציאה' AND posts.id = post_id)
  $$ LANGUAGE sql;

ALTER TABLE records.exit_requests ADD 
  CONSTRAINT check_if_post_is_exit_request CHECK (records.check_if_exit_req(post_id));

CREATE OR REPLACE VIEW "records"."completed_goals_students" AS
 SELECT users_roles.id,
    COALESCE(complete.completed_goals, (0)::bigint) AS completed_goals,
    COALESCE(total.total_goals, (0)::bigint) AS total_goals
   FROM ((( SELECT completed.student_user_role_id,
            count(*) AS completed_goals
           FROM records.goals completed
          WHERE (completed.accomplished = true)
          GROUP BY completed.student_user_role_id) complete
     RIGHT JOIN ( SELECT DISTINCT goals.student_user_role_id,
            count(*) AS total_goals
           FROM records.goals
          GROUP BY goals.student_user_role_id) total ON ((complete.student_user_role_id = total.student_user_role_id)))
     RIGHT JOIN ( SELECT users_roles_1.id
           FROM core.users_roles users_roles_1
          WHERE (users_roles_1.role_id = 5)) users_roles ON ((users_roles.id = total.student_user_role_id)));