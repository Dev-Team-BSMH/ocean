SET
  CLIENT_ENCODING TO 'utf8';
---- eval ----
  --checks--
--inserted_grades--
ALTER TABLE eval.inserted_grades
ADD CONSTRAINT check_if_student
CHECK (core.check_if_student(student_id));
--
  -- Name: groups_eval_trees_by_ancestors; Type: VIEW; Schema: eval; Owner: devteam
  --
CREATE OR REPLACE VIEW eval.groups_eval_roots_by_ancestors
 AS
 SELECT group_ancestors.group_id,
    group_ancestors.ancestor_id AS group_tree_id,
    group_ancestors.depth,
    group_eval_roots.root_id
   FROM core.group_ancestors
     JOIN eval.group_eval_roots ON group_eval_roots.group_id = group_ancestors.ancestor_id
UNION
 SELECT groups.id AS group_id,
    groups.id AS group_tree_id,
    0 AS depth,
    group_eval_roots.root_id
   FROM core.groups
     JOIN eval.group_eval_roots ON groups.id = group_eval_roots.group_id
  ORDER BY 1, 3, 4;

  -- create view roots
  CREATE OR REPLACE VIEW eval.roots
 AS
 SELECT milestones.id,
    milestones.name
   FROM eval.milestones
  WHERE parent IS NULL
  GROUP BY milestones.id, milestones.name;

-- create view for all grades
  
CREATE VIEW eval.students_grades AS
SELECT id as grade_id,student_id, milestone_id,grade,created_at FROM eval.inserted_grades
UNION 
SELECT id as grade_id,student_id,milestone_id, grade,created_at FROM eval.calculated_grades;

CREATE OR REPLACE VIEW eval.leaves
 AS
SELECT milestones.id,milestones.name
FROM eval.milestones
WHERE milestones.id NOT IN
(SELECT DISTINCT milestones2.parent FROM eval.milestones as milestones2 WHERE milestones2.parent is not null);

  CREATE VIEW eval.grades_by_milestone_tree_predecessor AS
 SELECT group_ancestors.ancestor_id AS group_id,
    students_grades.milestone_id,
    avg(students_grades.grade) AS average_grade
   FROM core.group_ancestors
     JOIN core.users_roles_groups ON group_ancestors.group_id = users_roles_groups.group_id
     JOIN eval.group_eval_roots ON group_ancestors.ancestor_id = group_eval_roots.group_id
     JOIN eval.students_grades ON users_roles_groups.user_role_id = students_grades.student_id 
  GROUP BY group_ancestors.ancestor_id, students_grades.milestone_id;

CREATE OR REPLACE VIEW eval.milestone_ancestors
 AS
 WITH RECURSIVE milestone_ancestors(milestone_id, ancestor_id) AS (
         SELECT milestones.id AS milestone_id,
            milestones.parent AS ancestor_id,
            1 AS depth
           FROM eval.milestones
        UNION
         SELECT milestone_ancestors_1.milestone_id,
            milestones.parent AS ancestor_id,
            milestone_ancestors_1.depth + 1 AS depth
           FROM eval.milestones
             JOIN milestone_ancestors milestone_ancestors_1 ON milestone_ancestors_1.ancestor_id = milestones.id
        )
 SELECT milestone_ancestors.milestone_id,
    milestone_ancestors.ancestor_id,
    milestone_ancestors.depth
   FROM milestone_ancestors
   WHERE milestone_ancestors.ancestor_id IS NOT NULL
UNION
 SELECT milestones.id AS milestone_id,
    milestones.id AS ancestor_id,
    0 AS depth
   FROM eval.milestones
  ORDER BY 1, 3;

  CREATE OR REPLACE VIEW eval.parent_of_leaves AS
SELECT milestones.parent AS parent_of_leaf_id,
    leaves.id AS leaf_id
   FROM eval.leaves
     JOIN eval.milestones ON milestones.id = leaves.id;
--
  -- Name: check_if_critirion(integer); Type: FUNCTION; Schema: eval; Owner: devteam
  --
CREATE OR REPLACE FUNCTION eval.check_if_critirion(
	milestone_id integer)
    RETURNS boolean
    LANGUAGE 'sql'

    COST 100
    VOLATILE 
AS $BODY$
SELECT
  EXISTS(
    SELECT
      *
    FROM(
        SELECT
          tag as tag
        FROM eval.milestones
        JOIN eval.milestones_tags ON milestones_tags.milestone_id = milestones.id
        JOIN eval.tags ON milestones_tags.tag_id = tags.id
        WHERE
          milestones.id = milestone_id
      ) as critirion
    WHERE
      tag = 'קריטריון'
  ) $BODY$;

ALTER TABLE eval.milestones_descriptions
ADD
  CONSTRAINT check_if_critirion CHECK (eval.check_if_critirion(milestone_id)) NOT VALID;
--
  -- Name: check_if_leaf(integer); Type: FUNCTION; Schema: eval; Owner: devteam
  --
CREATE OR REPLACE FUNCTION eval.check_if_leaf(
	milestone_id integer)
    RETURNS boolean
    LANGUAGE 'sql'

    COST 100
    VOLATILE 
AS $BODY$
SELECT
  NOT EXISTS (
    SELECT
      *
    FROM eval.milestones as parents
    JOIN eval.milestones as children ON parents.id = children.parent
    WHERE
      parents.id = milestone_id
  ) $BODY$;
  
ALTER TABLE eval.milestones_descriptions
ADD
  CONSTRAINT check_if_leaf CHECK (eval.check_if_leaf(milestone_id)) NOT VALID;
--
  -- Name: check_if_root(integer, integer); Type: FUNCTION; Schema: eval; Owner: devteam
  --
CREATE OR REPLACE FUNCTION eval.check_if_root(
	milestone_id integer)
    RETURNS boolean
    LANGUAGE 'sql'

    COST 100
    VOLATILE 
AS $BODY$
SELECT
  EXISTS (
    SELECT
      roots.id
    FROM eval.roots roots
    WHERE
      roots.id = milestone_id
  ) $BODY$;

CREATE OR REPLACE VIEW eval.milestones_root_ancestor AS
SELECT
      DISTINCT ON(milestone_id) milestone_id,
      ancestor_id as root_id
    FROM (
        SELECT
          milestone_id,
          ancestor_id,
          max(depth) AS depth
        FROM eval.milestone_ancestors
        GROUP BY
          milestone_id,
          ancestor_id
      ) AS roots
    ORDER BY
      milestone_id,
      depth DESC;

-- students_progress view
CREATE OR REPLACE VIEW eval.students_progess
SELECT id, student_id, milestone_id, cast(
  case
    when grade <> 0 then cast(case when grade = 100 then 1 else 0.5 end as numeric) else 0
  end
as numeric) as progess, author_id, created_at
FROM eval.inserted_grades