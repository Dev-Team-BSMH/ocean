SET CLIENT_ENCODING TO 'utf8';
---- api_all ----
DROP SCHEMA IF EXISTS api_all cascade;
CREATE SCHEMA IF NOT EXISTS api_all;

--@ CORE @--
-- Groups (All)
CREATE OR REPLACE VIEW api_all.groups AS
SELECT groups.*, group_types.type as group_type_name, groups_period.start_date,
       groups_period.end_date, parent as parent_id
FROM core.groups
LEFT JOIN core.edges as parent ON parent.child = groups.id
JOIN core.group_types ON group_types.id = groups.group_type_id
LEFT JOIN core.groups_period ON groups_period.group_id = groups.id
ORDER BY id;

-- Madors
CREATE OR REPLACE VIEW api_all.madors AS
SELECT madors.id, madors.name, unit.id as unit_id, unit.name as unit_name
FROM api_all.groups as madors
JOIN api_all.groups as unit ON unit.parent_id is null AND unit.name = 'בסמ"ח'
WHERE madors.group_type_id=7
ORDER BY id;

-- Megamot
CREATE OR REPLACE VIEW api_all.megamot AS
SELECT megamot.id, megamot.name, madors.id as mador_id, madors.name as mador_name, madors.unit_id, madors.unit_name
FROM api_all.groups as megamot
JOIN api_all.madors ON madors.id = megamot.parent_id
WHERE megamot.group_type_id=2
ORDER BY id;

-- Cycles
CREATE OR REPLACE VIEW api_all.cycles AS
SELECT cycles.id, cycles.name, groups_period.start_date, groups_period.end_date,
       megamot.id as megama_id, megamot.name as megama_name,
       megamot.mador_id, megamot.mador_name, megamot.unit_id, megamot.unit_name
FROM api_all.groups as cycles
JOIN api_all.megamot ON megamot.id = cycles.parent_id
LEFT JOIN core.groups_period ON groups_period.group_id = cycles.id
WHERE cycles.group_type_id=4
ORDER BY id;

-- Courses
CREATE OR REPLACE VIEW api_all.courses AS
SELECT courses.id, courses.name, groups_period.start_date, groups_period.end_date,
       cycles.id as cycle_id, cycles.name as cycle_name,
       megamot.id as megama_id, megamot.name as megama_name,
       megamot.mador_id, megamot.mador_name, megamot.unit_id, megamot.unit_name
FROM api_all.groups as courses
LEFT JOIN api_all.cycles ON cycles.id = courses.parent_id
JOIN api_all.megamot ON megamot.id = courses.parent_id OR megamot.id = cycles.megama_id
LEFT JOIN core.groups_period ON groups_period.group_id = courses.id
WHERE courses.group_type_id=5
ORDER BY id;

-- Departments
CREATE OR REPLACE VIEW api_all.departments AS
SELECT departments.id, departments.name,
       courses.id as course_id, courses.name as course_name,
       courses.cycle_id as cycle_id, courses.cycle_name as cycle_name,
       courses.megama_id as megama_id, courses.megama_name as megama_name,
       courses.mador_id, courses.mador_name, courses.unit_id, courses.unit_name
FROM api_all.groups as departments
JOIN api_all.courses ON courses.id = departments.parent_id
WHERE departments.group_type_id = 3
ORDER BY id;

-- Teams
CREATE OR REPLACE VIEW api_all.teams AS
SELECT teams.id, teams.name,
       courses.id as course_id, courses.name as course_name,
       courses.cycle_id as cycle_id, courses.cycle_name as cycle_name,
       courses.megama_id as megama_id, courses.megama_name as megama_name,
       courses.mador_id, courses.mador_name, courses.unit_id, courses.unit_name
FROM api_all.groups as teams
JOIN api_all.courses ON courses.id = teams.parent_id
WHERE teams.group_type_id = 1
ORDER BY id;

-- Departments and Teams
CREATE OR REPLACE VIEW api_all.departments_and_teams AS
SELECT *
FROM api_all.departments
UNION
SELECT *
FROM api_all.teams
ORDER BY id;

-- Assignments (referred from the user views via user_id)
CREATE OR REPLACE VIEW api_all.assignments AS
SELECT DISTINCT assigns.id as assigns_id, users.id as user_id, users_roles.id as user_role_id,  roles.id as role_id, roles.role as role_name,
                users_roles.details, assigns.group_id, groups.name as group_name, groups.group_type_id,
                group_types.type as group_type_name, departments.id as department_id, teams.id as team_id, departments_and_teams.id as
                department_or_team_id, courses.id as course_id, cycles.id as cycle_id, megamot.id as megama_id, madors.id as mador_id
FROM core.users_roles
JOIN core.users ON users.id = users_roles.user_id
JOIN core.roles ON roles.id = users_roles.role_id
JOIN core.assigns ON assigns.user_role_id = users_roles.id
JOIN core.groups ON assigns.group_id = groups.id
JOIN core.group_types ON group_types.id = "groups".group_type_id
JOIN core.group_ancestors ON group_ancestors.group_id = assigns.group_id
LEFT JOIN api_all.teams ON teams.id = assigns.group_id
LEFT JOIN api_all.departments ON departments.id = assigns.group_id
LEFT JOIN api_all.departments_and_teams ON departments_and_teams.id = assigns.group_id
LEFT JOIN api_all.courses ON (courses.id = assigns.group_id AND role_id = 3) OR courses.id = group_ancestors.ancestor_id
LEFT JOIN api_all.cycles ON cycles.id = group_ancestors.ancestor_id
JOIN api_all.megamot ON (megamot.id = assigns.group_id AND role_id = 2) OR megamot.id = courses.megama_id
JOIN api_all.madors ON madors.id = megamot.mador_id
ORDER BY assigns.id;

-- Students
CREATE OR REPLACE VIEW api_all.students AS
SELECT
soldier_id, first_name, last_name, gender, users.id as user_id,
users_roles.details as student_details,
users_roles.details->>'student_number' as student_number,
users_roles_termination.reason as if_expelled_reason
FROM core.users
JOIN core.users_roles ON users_roles.user_id = users.id
LEFT JOIN core.users_roles_termination ON users_roles_termination.user_role_id = users_roles.id
WHERE users_roles.role_id=5
GROUP BY soldier_id, first_name, last_name, gender, users.id, users_roles_termination.reason, users_roles.details;

-- Instructors
CREATE OR REPLACE VIEW api_all.instructors AS
SELECT
soldier_id, first_name, last_name, gender, users.id as user_id
FROM core.users
JOIN core.users_roles ON users_roles.user_id = users.id
WHERE users_roles.role_id=1
GROUP BY soldier_id, first_name, last_name, gender, users.id;

-- Makasim
CREATE OR REPLACE VIEW api_all.makasim AS
SELECT
soldier_id, first_name, last_name, gender, users.id as user_id
FROM core.users
JOIN core.users_roles ON users_roles.user_id = users.id
WHERE users_roles.role_id=3
GROUP BY soldier_id, first_name, last_name, gender, users.id;

-- Tzapaot
CREATE OR REPLACE VIEW api_all.tzapaot AS
SELECT
soldier_id, first_name, last_name, gender, users.id as user_id
FROM core.users
JOIN core.users_roles ON users_roles.user_id = users.id
WHERE users_roles.role_id=2
GROUP BY soldier_id, first_name, last_name, gender, users.id;

-- Users (All)
CREATE OR REPLACE VIEW api_all.users AS
SELECT soldier_id, first_name, last_name, gender, user_id
FROM api_all.students
UNION
SELECT instructors.*
FROM api_all.instructors
UNION
SELECT makasim.*
FROM api_all.makasim
UNION
SELECT tzapaot.*
FROM api_all.tzapaot;

--@ EVAL @--
-- All
CREATE OR REPLACE VIEW api_all.milestones AS 
SELECT milestones.id, name, parent as parent_id, weight, milestones.order, scale, root_id, array_agg(to_jsonb(tags.*)) as tags
FROM eval.milestones
LEFT JOIN eval.milestones_tags ON milestones.id = milestones_tags.milestone_id
LEFT JOIN eval.tags ON tags.id = milestones_tags.tag_id
JOIN eval.milestones_root_ancestor ON milestones_root_ancestor.milestone_id = milestones.id
GROUP BY milestones.id, root_id;

-- Leaves
CREATE OR REPLACE VIEW api_all.leaves AS 
SELECT milestones.*
FROM api_all.milestones
JOIN eval.milestones_tags ON milestones_tags.milestone_id = milestones.id
WHERE NOT (milestones.id IN (
    SELECT DISTINCT milestones.parent_id
    FROM api_all.milestones milestones
    WHERE milestones.parent_id IS NOT NULL))
AND tag_id <> 1 AND tag_id <> 6;

--- Milestone types: ---
-- criterions
CREATE OR REPLACE VIEW api_all.criterions AS 
SELECT *, to_jsonb(criteria.*) as json FROM
(SELECT criterion.id, criterion.name, criterion.root_id, weight, "order", scale, tags,
meeting_tag.milestone_id as meeting_id,
test_tag.milestone_id as test_id
FROM api_all.milestones as criterion
LEFT JOIN eval.milestones_tags as test_tag ON criterion.parent_id = test_tag.milestone_id
AND (test_tag.tag_id = 2 OR test_tag.tag_id = 3 OR test_tag.tag_id = 7 OR test_tag.tag_id = 8)
LEFT JOIN eval.milestones_tags as meeting_tag ON criterion.parent_id = meeting_tag.milestone_id AND meeting_tag.tag_id = 5
JOIN eval.milestones_tags as criterion_tags ON criterion_tags.milestone_id = criterion.id
WHERE criterion_tags.tag_id = 4) as criteria;
-- meetings
CREATE OR REPLACE VIEW api_all.meetings AS
SELECT id, name, root_id, weight, meetings.order, scale, tags, test_id, to_jsonb(meetings.*) as json FROM
(
SELECT meeting.id, meeting.name, meeting.root_id, meeting.weight, meeting.order, meeting.scale, meeting.tags,
test_tag.milestone_id as test_id, array_agg(DISTINCT criterions.json) as criterions
FROM api_all.milestones as meeting
LEFT JOIN eval.milestones_tags as test_tag ON meeting.parent_id = test_tag.milestone_id
AND (test_tag.tag_id = 2 OR test_tag.tag_id = 3 OR test_tag.tag_id = 7 OR test_tag.tag_id = 8)
LEFT JOIN api_all.criterions ON criterions.meeting_id = meeting.id
JOIN eval.milestones_tags as meeting_tags ON meeting_tags.milestone_id = meeting.id
WHERE meeting_tags.tag_id = 5
GROUP BY meeting.id, meeting.name, meeting.root_id, meeting.weight, meeting.order, meeting.scale, meeting.tags, test_tag.milestone_id
) as meetings;
-- tests: מבחן, מבחן חוזר, תר"ץ, מדרגה
CREATE OR REPLACE VIEW api_all.tests AS
SELECT id, name, root_id, weight, tests.order, scale, tags, profession_id, to_jsonb(tests.*) as json FROM
    (SELECT test.id, test.name, test.root_id, test.weight, test.order, test.scale, test.tags,
            profession_tag.milestone_id as profession_id, array_agg(DISTINCT meetings.json) as meetings, array_agg(DISTINCT criterions.json) as criterions
     FROM api_all.milestones as test
     LEFT JOIN eval.milestones_tags as profession_tag ON test.parent_id = profession_tag.milestone_id AND (profession_tag.tag_id = 1)
     LEFT JOIN api_all.criterions ON criterions.test_id = test.id
     LEFT JOIN api_all.meetings ON meetings.test_id = test.id
     JOIN eval.milestones_tags as test_tags ON test_tags.milestone_id = test.id
     WHERE test_tags.tag_id = 2 OR test_tags.tag_id = 3 OR test_tags.tag_id = 7 OR test_tags.tag_id = 8
     GROUP BY test.id, test.name, test.root_id, test.weight, test.order, test.scale, test.tags, profession_tag.milestone_id
    ) as tests;
-- professions
CREATE OR REPLACE VIEW api_all.professions AS
SELECT id, name, root_id, weight, professions.order, scale, tags, to_jsonb(professions.*) as json FROM
(SELECT profession.id, profession.name, profession.root_id, profession.weight, profession.order,
profession.scale, profession.tags, array_agg(DISTINCT tests.json) as tests
FROM api_all.milestones as profession
LEFT JOIN eval.milestones_tags as root_tag ON profession.parent_id = root_tag.milestone_id AND (root_tag.tag_id = 6)
LEFT JOIN api_all.tests ON tests.profession_id = profession.id
JOIN eval.milestones_tags as profession_tag ON profession_tag.milestone_id = profession.id
WHERE profession_tag.tag_id = 1
GROUP BY profession.id, profession.name, profession.root_id, profession.weight, profession.order, profession.scale, profession.tags, root_tag.milestone_id
) as professions;
-- Tree roots
CREATE OR REPLACE VIEW api_all.tree_roots AS
SELECT id, name, course_id, tags, to_jsonb(root.*) as tree FROM
(SELECT root.id, root.name, courses.id as course_id, root.tags, array_agg(DISTINCT professions.json) as professions
FROM api_all.milestones as root
JOIN eval.group_eval_roots ON group_eval_roots.root_id = root.id
LEFT JOIN api_all.courses ON courses.id = group_eval_roots.group_id
LEFT JOIN api_all.professions ON professions.root_id = root.id
WHERE parent_id is null
GROUP BY root.id, root.name, courses.id, root.tags
) as root;

--- Grades ---
CREATE OR REPLACE VIEW api_all.grades AS
SELECT *
FROM eval.calculated_grades;
