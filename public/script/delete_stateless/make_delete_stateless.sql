SELECT 'DROP FUNCTION IF EXISTS ' || ns.nspname || '.' || proname 
       || ' CASCADE;' as stateless_item
FROM pg_proc INNER JOIN pg_namespace ns ON (pg_proc.pronamespace = ns.oid)
WHERE ns.nspname IN ('core','eval','records','mantle', 'api_all', 'api_now')
UNION
SELECT 'DROP VIEW IF EXISTS ' || table_schema || '.' || table_name || ' CASCADE;' as stateless_item
FROM information_schema.views
WHERE table_schema NOT IN ('pg_catalog', 'information_schema','hdb_catalog','hdb_views')
AND table_name !~ '^pg_';