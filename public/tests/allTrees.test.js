import  { gql } from "apollo-boost";
import de from 'dotenv';
import client from "./client.js"

de.config();
test('fetch data', async () => {
    jest.setTimeout(30000);
    await client
      .query({
        query: gql`
        query($groupId: Int) {
          AllTrees: eval_trees(
            where: { group_evaluation_trees: { group_id: { _eq: $groupId } } }
          ) {
            id
            name
          }
        }
      ` })
      .then(result => expect(result.data).toEqual(expect.any(Object)));
  })

