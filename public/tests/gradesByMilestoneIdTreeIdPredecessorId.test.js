import  { gql } from "apollo-boost";
import de from 'dotenv';
import client from "./client.js"

de.config();
test('fetch data', async () => {
    jest.setTimeout(30000);
    await client
      .query({
        query: gql`
        query($groupId: Int, $treeId: Int, $milestones: [Int!]) {
          AverageGrade: eval_grades_by_milestone_tree_predecessor(
            where: {
              _and: [
                { group_id: { _eq: $groupId } }
                { tree_id: { _eq: $treeId } }
                { milestone_id: { _in: $milestones } }
              ]
            }
          ) {
            average_grade
            milestone {
              id
              name
            }
          }
        }
      ` })
      .then(result => expect(result.data).toEqual(expect.any(Object)));
  })