import  { gql } from "apollo-boost";
import de from 'dotenv';
import client from "./client.js"

de.config();
test('fetch data', async () => {
    jest.setTimeout(30000);
    await client
      .query({
        query: gql`
        query($milestoneId: Int) {
          milestoneChilds: eval_milestone_ancestors(
            where: {
              _and: [{ ancestor_id: { _eq: $milestoneId } }, { depth: { _eq: 1 } }]
            }
          ) {
            milestone_id
          }
        }
      ` })
      .then(result => expect(result.data).toEqual(expect.any(Object)));
  })