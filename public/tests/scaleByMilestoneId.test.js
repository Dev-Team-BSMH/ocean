import  { gql } from "apollo-boost";
import de from 'dotenv';
import client from "./client.js"

de.config();
test('fetch data', async () => {
    jest.setTimeout(30000);
    await client
      .query({
        query: gql`
        query($milestoneId: Int) {
          Scale: eval_scales(where: { milestone_id: { _eq: $milestoneId } }) {
            scale
          }
        }
      ` })
      .then(result => expect(result.data).toEqual(expect.any(Object)));
  })