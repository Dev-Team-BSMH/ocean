import  { gql } from "apollo-boost";
import de from 'dotenv';
import client from "./client.js"

de.config();
test('fetch data', async () => {
    jest.setTimeout(30000);
    await client
      .query({
        query: gql`
        query($milestoneId: Int, $treeId: Int) {
          leafsByParent: eval_parents_of_leafs(
            where: {
              parent_of_leaf_id: { _eq: $milestoneId }
              tree_id: { _eq: $treeId }
            }
          ) {
            leaf {
              id
              name
              edges_by_child {
                order
              }
            }
            scale {
              scale
            }
          }
        }
      ` })
      .then(result => expect(result.data).toEqual(expect.any(Object)));
  })