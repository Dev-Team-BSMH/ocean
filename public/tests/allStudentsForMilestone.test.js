import  { gql } from "apollo-boost";
import de from 'dotenv';
import client from "./client.js"

de.config();
test('fetch data', async () => {
    jest.setTimeout(60000);
    await client
      .query({
        query: gql `
        query($groupId: Int, $milestoneId: Int, $treeId: Int) {
          allStudentsForMilestone: core_group_ancestors(
            where: {
              _or: [
                { group_id: { _eq: $groupId } }
                { ancestor_id: { _eq: $groupId } }
              ]
            }
            distinct_on: group_id
          ) {
            group {
              id
              assigns(where: { users_role: { role: { role: { _eq: "חניך" } } } }) {
                users_role {
                  id
                  details
                  grades(
                    where: {
                      _and: [
                        { tree_id: { _eq: $treeId } }
                        { milestone_id: { _eq: $milestoneId } }
                      ]
                    }
                  ) {
                    milestone_id
                    grade
                  }
                  user {
                    first_name
                    last_name
                  }
                }
              }
            }
          }
        }
      `})
      .then(result => expect(result.data).toEqual(expect.any(Object)));
  })