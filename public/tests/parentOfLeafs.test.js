import  { gql } from "apollo-boost";
import de from 'dotenv';
import client from "./client.js"

de.config();
test('fetch data', async () => {
    jest.setTimeout(30000);
    await client
      .query({
        query: gql`
        query($milestoneId: Int) {
          ParentOfLeafs: eval_parents_of_leafs(
            where: { parent_of_leaf_id: { _eq: $milestoneId } }
          ) {
            parent_of_leaf_id
            leaf_id
            tree_id
          }
        }
      ` })
      .then(result => expect(result.data).toEqual(expect.any(Object)));
  })