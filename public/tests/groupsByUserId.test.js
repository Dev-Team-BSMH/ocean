import  { gql } from "apollo-boost";
import de from 'dotenv';
import client from "./client.js"

de.config();
test('fetch data', async () => {
    jest.setTimeout(30000);
    await client
      .query({
        query: gql`
        query($userId: Int) {
          groups: core_group_for_soldier_id(
            where: {
              group: { group_type: { type: { _eq: "מחזור" } } }
              soldier_id: { _eq: $userId }
            }
          ) {
            group {
              id
              name
              groups_period {
                start_date
                end_date
              }
              edgesByParent {
                childMilestones {
                  id
                  name
                }
                parentMilestone {
                  id
                  name
                }
              }
            }
          }
        }
      ` })
      .then(result => expect(result.data).toEqual(expect.any(Object)));
  })