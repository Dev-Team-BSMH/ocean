import  { gql } from "apollo-boost";
import de from 'dotenv';
import client from "./client.js"

de.config();
test('fetch data', async () => {
    jest.setTimeout(30000);
    await client
      .query({
        query: gql`
        query($groupId: Int) {
          GroupTrees: eval_groups_eval_trees_by_ancestors(
            where: { _and: [{ group_id: { _eq: $groupId } }, { depth: { _eq: 0 } }] }
          ) {
            group_id
            root_id
            tree_id
            depth
          }
          AncestorsTrees: eval_groups_eval_trees_by_ancestors(
            where: { _and: [{ group_id: { _eq: $groupId } }, { depth: { _neq: 0 } }] }
          ) {
            root_id
            group_id
            tree_id
            depth
            ancestor {
              ancestor_id
              depth
            }
          }
        }
      ` })
      .then(result => expect(result.data).toEqual(expect.any(Object)));
  })