import  { gql } from "apollo-boost";
import de from 'dotenv';
import client from "./client.js"

de.config();
test('fetch data', async () => {
    jest.setTimeout(30000);
    await client
      .query({
        query: gql `
        query($groupId: Int, $treeId: Int, $milestoneId: Int) {
          grades: core_users_roles(
            where: {
              _or: [
                { assigns: { group: { edges: { parent: { _eq: $groupId } } } } }
                { assigns: { group_id: { _eq: $groupId } } }
              ]
              role: { role: { _eq: "חניך" } }
            }
          ) {
            user {
              first_name
              last_name
              users_roles {
                details
              }
            }
            id
            grades(
              where: {
                _and: [
                  { milestone_id: { _eq: $milestoneId } }
                  { tree_id: { _eq: $treeId } }
                ]
              }
            ) {
              grade
            }
          }
        }
      `})
      .then(result => expect(result.data).toEqual(expect.any(Object)));
  })