import  { gql } from "apollo-boost";
import de from 'dotenv';
import client from "./client.js"

de.config();
test('fetch data', async () => {
    jest.setTimeout(30000);
    await client
      .query({
        query: gql`
        query($groupId: Int, $treeId: Int) {
          rootId: eval_group_evaluation_trees(
            where: {
              group_id: { _eq: $groupId }
              _and: { tree_id: { _eq: $treeId } }
            }
          ) {
            root_id
          }
        }
      ` })
      .then(result => expect(result.data).toEqual(expect.any(Object)));
  })