import ApolloClient from "apollo-boost";
import fetch from 'node-fetch';

const client = new ApolloClient({
  uri: process.env.API_ENDPOINT || "http://localhost:5555/v1/graphql",
  fetch: fetch,
  request: async operation => {
    operation.setContext({
      headers: {
        "content-type": "application/json",
        "x-hasura-admin-secret": process.env.ADMIN_SECRET || "developmenteam"
      }
    });
  },
});

export default client;