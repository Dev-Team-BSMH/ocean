import  { gql } from "apollo-boost";
import de from 'dotenv';
import client from "./client.js"

de.config();
test('fetch data', async () => {
    jest.setTimeout(60000);
    await client
      .query({
        query: gql`
        query($students: [Int!], $treeId: Int) {
          sheetGrades: core_users_roles(where: { id: { _in: $students } }) {
            id
            user {
              first_name
              last_name
            }
            grades(where: { tree_id: { _eq: $treeId } }) {
              grade
              milestone {
                edges_by_child(where: { tree_id: { _eq: $treeId } }) {
                  parent
                }
                id
                name
              }
            }
          }
        }
      ` })
      .then(result => expect(result.data).toEqual(expect.any(Object)));
  })