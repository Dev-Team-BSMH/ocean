import  { gql } from "apollo-boost";
import de from 'dotenv';
import client from "./client.js"

de.config();
test('fetch data', async () => {
    jest.setTimeout(30000);
    await client
      .query({
        query: gql`
        query($treeId: Int) {
          maxOrder: eval_edges_aggregate(where: { tree_id: { _eq: $treeId } }) {
            aggregate {
              max {
                order
              }
            }
          }
        }
      ` })
      .then(result => expect(result.data).toEqual(expect.any(Object)));
  })