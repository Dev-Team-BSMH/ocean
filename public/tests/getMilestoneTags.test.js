import  { gql } from "apollo-boost";
import de from 'dotenv';
import client from "./client.js"

de.config();
test('fetch data', async () => {
    jest.setTimeout(30000);
    await client
      .query({
        query: gql`
        {
          milestoneTags: eval_tags {
            id
            tag
          }
        }
      ` })
      .then(result => expect(result.data).toEqual(expect.any(Object)));
  })