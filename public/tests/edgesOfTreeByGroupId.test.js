import  { gql } from "apollo-boost";
import de from 'dotenv';
import client from "./client.js"

de.config();
test('fetch data', async () => {
    jest.setTimeout(30000);
    await client
      .query({
        query: gql`
        query($groupId: Int, $treeId: Int) {
          Nodes: eval_group_evaluation_trees(
            where: {
              group_id: { _eq: $groupId }
              _and: { tree_id: { _eq: $treeId } }
            }
          ) {
            milestone {
              ...MilestoneDetails
              edges_by_parent(
                order_by: { order: asc }
                where: { tree_id: { _eq: $treeId } }
              ) {
                weight
                order
                child_milestone {
                  ...MaxOrder
                  ...MilestoneDetails
                  edges_by_parent(
                    order_by: { order: asc }
                    where: { tree_id: { _eq: $treeId } }
                  ) {
                    weight
                    order
                    child_milestone {
                      ...MaxOrder
                      ...MilestoneDetails
                      edges_by_parent(
                        order_by: { order: asc }
                        where: { tree_id: { _eq: $treeId } }
                      ) {
                        weight
                        order
                        child_milestone {
                          ...MaxOrder
                          ...MilestoneDetails
                          edges_by_parent(
                            order_by: { order: asc }
                            where: { tree_id: { _eq: $treeId } }
                          ) {
                            weight
                            order
                            child_milestone {
                              ...MaxOrder
                              ...MilestoneDetails
                              edges_by_parent(
                                order_by: { order: asc }
                                where: { tree_id: { _eq: $treeId } }
                              ) {
                                weight
                                order
                                child_milestone {
                                  ...MaxOrder
                                  ...MilestoneDetails
                                  edges_by_parent(
                                    where: { tree_id: { _eq: $treeId } }
                                    order_by: { order: asc }
                                  ) {
                                    weight
                                    order
                                    child_milestone {
                                      ...MaxOrder
                                      ...MilestoneDetails
                                      edges_by_parent(
                                        where: { tree_id: { _eq: $treeId } }
                                        order_by: { order: asc }
                                      ) {
                                        weight
                                        order
                                        child_milestone {
                                          ...MaxOrder
                                          ...MilestoneDetails
                                          edges_by_parent(
                                            where: { tree_id: { _eq: $treeId } }
                                            order_by: { order: asc }
                                          ) {
                                            weight
                                            order
                                            child_milestone {
                                              ...MaxOrder
                                              ...MilestoneDetails
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      
        fragment MilestoneDetails on eval_milestones {
          id
          name
        }
      
        fragment MaxOrder on eval_milestones {
          edges_by_parent_aggregate {
            aggregate {
              max {
                order
              }
            }
          }
        }
      ` })
      .then(result => expect(result.data).toEqual(expect.any(Object)));
  })