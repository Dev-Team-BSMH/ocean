import  { gql } from "apollo-boost";
import de from 'dotenv';
import client from "./client.js"

de.config();
test('fetch data', async () => {
    jest.setTimeout(60000);
    await client
      .query({
        query: gql`
        query($currentNode: Int, $treeId: Int) {
          EditingScreenCurrentNode: eval_milestones(
            where: { id: { _eq: $currentNode } }
          ) {
            id
            name
            parents_of_leafs {
              scale {
                scale
              }
            }
            grades {
              grade
            }
            milestones_tags {
              tag {
                id
                tag
              }
            }
            edges_by_parent(where: { tree_id: { _eq: $treeId } }) {
              weight
              child_milestone {
                id
                name
              }
            }
            edges_by_child(where: { tree_id: { _eq: $treeId } }) {
              id
              weight
              tree_id
              parent_milestone {
                id
                name
                group_evaluation_trees(where: { tree_id: { _eq: $treeId } }) {
                  root_id
                }
              }
              child_milestone {
                name
                id
              }
            }
          }
        }
      ` })
      .then(result => expect(result.data).toEqual(expect.any(Object)));
  })