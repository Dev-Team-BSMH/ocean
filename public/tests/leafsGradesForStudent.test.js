import  { gql } from "apollo-boost";
import de from 'dotenv';
import client from "./client.js"

de.config();
test('fetch data', async () => {
    jest.setTimeout(30000);
    await client
      .query({
        query: gql`
        query($groupId: Int, $treeId: Int, $parentId: Int) {
          leafsGradesForStudent: core_users_roles(
            where: {
              assigns: { users_role: { role: { role: { _eq: "חניך" } } } }
              _or: [
                { assigns: { group: { edges: { parent: { _eq: $groupId } } } } }
                { assigns: { group_id: { _eq: $groupId } } }
              ]
            }
          ) {
            id
            user {
              first_name
              last_name
              users_roles(
                where: {
                  _or: [
                    { assigns: { group: { edges: { parent: { _eq: $groupId } } } } }
                    { assigns: { group_id: { _eq: $groupId } } }
                  ]
                }
              ) {
                details
              }
            }
            grades(
              where: {
                milestone: {
                  edges_by_child: {
                    tree_id: { _eq: $treeId }
                    parent: { _eq: $parentId }
                  }
                }
              }
            ) {
              milestone {
                id
                edges_by_child(where: { tree_id: { _eq: $treeId } }) {
                  order
                }
              }
              grade
              grade_id
            }
          }
        }
      ` })
      .then(result => expect(result.data).toEqual(expect.any(Object)));
  })