-- Deploy pacific-ocean:adding-refactored-grades-tables to pg

BEGIN;
SET
  CLIENT_ENCODING TO 'utf8';
    CREATE TABLE eval.inserted_grades
    (
        "id" serial NOT NULL PRIMARY KEY,
        "student_id" integer NOT NULL REFERENCES core.users_roles(id) ON UPDATE CASCADE ON DELETE CASCADE,
        "milestone_id" integer NOT NULL REFERENCES eval.milestones(id) ON UPDATE CASCADE ON DELETE CASCADE,
        "tree_id" integer NOT NULL REFERENCES eval.trees(id) ON UPDATE CASCADE ON DELETE CASCADE,
        "grade" double precision NOT NULL,
        "created_at" timestamp without time zone DEFAULT now()
    );

    ALTER TABLE eval.inserted_grades
        ADD CONSTRAINT inserted_grades_milestone_student_and_tree_unique UNIQUE (milestone_id, student_id, tree_id);

    CREATE TABLE eval.calculated_grades
    (
        "id" serial NOT NULL PRIMARY KEY,
        "student_id" integer NOT NULL REFERENCES core.users_roles(id) ON UPDATE CASCADE ON DELETE CASCADE,
        "milestone_id" integer NOT NULL REFERENCES eval.milestones(id) ON UPDATE CASCADE ON DELETE CASCADE,
        "tree_id" integer NOT NULL REFERENCES eval.trees(id) ON UPDATE CASCADE ON DELETE CASCADE,
        "grade" double precision NOT NULL,
        "created_at" timestamp without time zone DEFAULT now()
    );

     ALTER TABLE eval.calculated_grades
        ADD CONSTRAINT calculated_grades_milestone_student_and_tree_unique UNIQUE (milestone_id, student_id, tree_id);
        
     ALTER TABLE eval.milestones_descriptions 
      DROP CONSTRAINT milestones_descriptions_milestone_id_fkey;

    ALTER TABLE eval.milestones_descriptions
    ADD CONSTRAINT milestones_descriptions_milestone_id_fkey FOREIGN KEY (milestone_id)
    REFERENCES eval.milestones(id)
    ON UPDATE CASCADE
    ON DELETE CASCADE;


      CREATE VIEW core.group_ancestors AS WITH RECURSIVE group_ancestors(group_id, ancestor_id) AS (
    SELECT
      groups.id AS group_id,
      edges.parent AS ancestor_id,
      1 AS depth
    FROM (
        core.groups
        JOIN core.edges ON ((edges.child = groups.id))
      )
    UNION
    SELECT
      group_ancestors_1.group_id,
      edges.parent AS ancestor_id,
      (group_ancestors_1.depth + 1) AS depth
    FROM (
        (
          core.groups
          JOIN group_ancestors group_ancestors_1 ON ((group_ancestors_1.group_id = groups.id))
        )
        JOIN core.edges ON ((edges.child = group_ancestors_1.ancestor_id))
      )
  )
SELECT
  group_ancestors.group_id,
  group_ancestors.ancestor_id,
  group_ancestors.depth
FROM group_ancestors
UNION
SELECT
  groups.id AS group_id,
  groups.id AS ancestor_id,
  0 AS depth
FROM core.groups
ORDER BY
  1,
  2;
      CREATE VIEW core.students AS
SELECT
  users_roles.id,
  users_roles.user_id
FROM (
    core.users_roles
    JOIN core.roles ON (
        (
          (roles.id = users_roles.role_id)
          AND (roles.role = 'חניך' :: text)
        )
      )
  );
--
  -- Name: users_roles_groups; Type: VIEW; Schema: core
  --
  CREATE VIEW core.users_roles_groups AS
SELECT
  users_roles.id AS user_role_id,
  assigns.group_id
FROM (
    core.users_roles
    JOIN core.assigns ON ((assigns.user_role_id = users_roles.id))
  )
ORDER BY
  users_roles.id,
  assigns.group_id;
      CREATE VIEW eval.groups_eval_trees_by_ancestors AS
SELECT
  group_ancestors.group_id,
  group_ancestors.ancestor_id AS group_tree_id,
  group_ancestors.depth,
  group_evaluation_trees.root_id,
  group_evaluation_trees.tree_id
FROM (
    core.group_ancestors
    JOIN eval.group_evaluation_trees ON (
        (
          group_evaluation_trees.group_id = group_ancestors.ancestor_id
        )
      )
  )
UNION
SELECT
  groups.id AS group_id,
  groups.id AS group_tree_id,
  0 AS depth,
  group_evaluation_trees.root_id,
  group_evaluation_trees.tree_id
FROM (
    core.groups
    JOIN eval.group_evaluation_trees ON ((groups.id = group_evaluation_trees.group_id))
  )
ORDER BY
  1,
  3,
  4;
    CREATE VIEW eval.actual_student_weight AS
SELECT
  students.id AS student_id,
  edges.id AS edge_id,
  groups_eval_trees_by_ancestors.group_tree_id,
  COALESCE(students_edges_weights.weight, edges.weight) AS actual_weight
FROM (
    (
      (
        (
          core.students
          JOIN core.assigns ON ((students.id = assigns.user_role_id))
        )
        JOIN eval.groups_eval_trees_by_ancestors ON (
            (
              groups_eval_trees_by_ancestors.group_id = assigns.group_id
            )
          )
      )
      JOIN eval.edges ON (
          (
            edges.tree_id = groups_eval_trees_by_ancestors.tree_id
          )
        )
    )
    LEFT JOIN eval.students_edges_weights ON (
        (
          (students_edges_weights.student_id = students.id)
          AND (students_edges_weights.edge_id = edges.id)
        )
      )
  )
ORDER BY
  students.id,
  edges.id,
  groups_eval_trees_by_ancestors.group_tree_id;
      CREATE VIEW eval.leafs_by_tree AS
SELECT
  milestones.id,
  milestones.name,
  edges.tree_id
FROM (
    eval.milestones
    JOIN eval.edges ON ((edges.child = milestones.id))
  )
WHERE
  (
    NOT (
      milestones.id IN (
        SELECT
          edges_1.parent AS milestone
        FROM (
            (
              eval.milestones milestones_1
              JOIN eval.edges edges_1 ON ((milestones_1.id = edges_1.parent))
            )
            JOIN eval.edges parents ON (
                (
                  (parents.child = edges_1.parent)
                  AND (parents.tree_id = edges_1.tree_id)
                )
              )
          )
      )
    )
  );
      CREATE VIEW eval.student_milestones_normalized_weights_by_siblings AS
SELECT
  actual_student_weight.student_id,
  actual_student_weight.edge_id,
  actual_student_weight.group_tree_id,
  (
    actual_student_weight.actual_weight / sum(sibling.actual_weight)
  ) AS normalized_weight
FROM (
    (
      (
        (
          eval.actual_student_weight
          JOIN eval.edges ON ((edges.id = actual_student_weight.edge_id))
        )
        JOIN eval.group_evaluation_trees ON (
            (
              (group_evaluation_trees.tree_id = edges.tree_id)
              AND (
                group_evaluation_trees.group_id = actual_student_weight.group_tree_id
              )
            )
          )
      )
      JOIN eval.edges siblings_edges ON (
          (
            (siblings_edges.parent = edges.parent)
            AND (siblings_edges.tree_id = edges.tree_id)
          )
        )
    )
    JOIN eval.actual_student_weight sibling ON (
        (
          (sibling.edge_id = siblings_edges.id)
          AND (
            sibling.student_id = actual_student_weight.student_id
          )
        )
      )
  )
WHERE
  (
    actual_student_weight.actual_weight > (0) :: double precision
  )
GROUP BY
  actual_student_weight.student_id,
  actual_student_weight.group_tree_id,
  edges.child,
  actual_student_weight.edge_id,
  actual_student_weight.actual_weight
ORDER BY
  actual_student_weight.student_id,
  actual_student_weight.edge_id,
  actual_student_weight.group_tree_id;
    CREATE VIEW eval.leafs_normalized_weight_by_upper_milestone AS WITH RECURSIVE leafs_normalized_weight_by_upper_milestone(
    student_id,
    tree_id,
    leaf_id,
    upper_milestone_id,
    normalized_weight
  ) AS (
    SELECT
      grades.student_id,
      edges.tree_id,
      edges.child AS leaf_id,
      edges.parent AS upper_milestone_id,
      smnwbs.normalized_weight
    FROM (
        (
          (
            (
              (
                (
                  eval.grades
                  JOIN core.students ON ((students.id = grades.student_id))
                )
                JOIN core.users_roles_groups ON ((users_roles_groups.user_role_id = students.id))
              )
              JOIN eval.leafs_by_tree leafs ON ((leafs.id = grades.milestone_id))
            )
            JOIN eval.edges ON ((edges.child = leafs.id))
          )
          JOIN eval.group_evaluation_trees ON ((group_evaluation_trees.tree_id = edges.tree_id))
        )
        JOIN eval.student_milestones_normalized_weights_by_siblings smnwbs ON (
            (
              (smnwbs.edge_id = edges.id)
              AND (smnwbs.student_id = grades.student_id)
            )
          )
      )
    UNION
    SELECT
      lnwbum1_1.student_id,
      lnwbum1_1.tree_id,
      lnwbum1_1.leaf_id,
      edges.parent AS upper_milestone_id,
      (
        lnwbum1_1.normalized_weight * smnwbs.normalized_weight
      ) AS normalized_weight
    FROM (
        (
          leafs_normalized_weight_by_upper_milestone lnwbum1_1
          JOIN eval.edges ON (
              (
                (edges.child = lnwbum1_1.upper_milestone_id)
                AND (edges.tree_id = lnwbum1_1.tree_id)
              )
            )
        )
        JOIN eval.student_milestones_normalized_weights_by_siblings smnwbs ON (
            (
              (smnwbs.edge_id = edges.id)
              AND (smnwbs.student_id = lnwbum1_1.student_id)
            )
          )
      )
  )
SELECT
  lnwbum1.student_id,
  lnwbum1.tree_id,
  lnwbum1.leaf_id,
  lnwbum1.upper_milestone_id,
  lnwbum1.normalized_weight
FROM leafs_normalized_weight_by_upper_milestone lnwbum1
ORDER BY
  lnwbum1.student_id,
  lnwbum1.tree_id,
  lnwbum1.leaf_id,
  lnwbum1.upper_milestone_id;
CREATE VIEW eval.student_total_children_weights AS
SELECT
  leafs_normalized_weight_by_upper_milestone.upper_milestone_id,
  leafs_normalized_weight_by_upper_milestone.student_id,
  sum(
    leafs_normalized_weight_by_upper_milestone.normalized_weight
  ) AS total_weights
FROM (
    eval.grades
    JOIN eval.leafs_normalized_weight_by_upper_milestone ON (
        (
          (
            leafs_normalized_weight_by_upper_milestone.leaf_id = grades.milestone_id
          )
          AND (
            leafs_normalized_weight_by_upper_milestone.student_id = grades.student_id
          )
        )
      )
  )
GROUP BY
  leafs_normalized_weight_by_upper_milestone.upper_milestone_id,
  leafs_normalized_weight_by_upper_milestone.student_id;
    CREATE VIEW eval.student_milestones_grades AS WITH scaled_grades AS (
        SELECT
        grades.student_id,
        grades.milestone_id,
        (
            (
            (grades.grade) :: real * (100) :: double precision
            ) / (COALESCE(scales.scale, 100)) :: double precision
        ) AS grade,
        grades.id AS grade_id
        FROM (
            eval.grades
            LEFT JOIN eval.scales ON ((scales.milestone_id = grades.milestone_id))
        )
    )
    SELECT
    scaled_grades.student_id,
    scaled_grades.milestone_id AS milestone_id,
    groups_eval_trees_by_ancestors.tree_id,
    scaled_grades.grade,
    edges.parent,
    scaled_grades.grade_id
    FROM (
        (
        (
            scaled_grades
            JOIN core.assigns ON (
                (assigns.user_role_id = scaled_grades.student_id)
            )
        )
        JOIN eval.groups_eval_trees_by_ancestors ON (
            (
                assigns.group_id = groups_eval_trees_by_ancestors.group_id
            )
            )
        )
        LEFT JOIN eval.edges ON (
            (
            (
                edges.tree_id = groups_eval_trees_by_ancestors.tree_id
            )
            AND (edges.child = scaled_grades.milestone_id)
            )
        )
    )
    UNION
    SELECT
    lnwbum1.student_id,
    lnwbum1.upper_milestone_id AS milestone_id,
    lnwbum1.tree_id,
    (
        sum(
        (
            lnwbum1.normalized_weight * (scaled_grades.grade) :: real
        )
        ) / student_total_children_weights.total_weights
    ) AS grade,
    edges.parent,
    min(scaled_grades.grade_id) AS grade_id
    FROM (
        (
        (
            eval.leafs_normalized_weight_by_upper_milestone lnwbum1
            JOIN scaled_grades ON (
                (
                (scaled_grades.student_id = lnwbum1.student_id)
                AND (scaled_grades.milestone_id = lnwbum1.leaf_id)
                )
            )
        )
        JOIN eval.student_total_children_weights ON (
            (
                (
                student_total_children_weights.upper_milestone_id = lnwbum1.upper_milestone_id
                )
                AND (
                student_total_children_weights.student_id = lnwbum1.student_id
                )
                AND (
                student_total_children_weights.total_weights <> (0) :: double precision
                )
            )
            )
        )
        LEFT JOIN eval.edges ON (
            (
            (edges.tree_id = lnwbum1.tree_id)
            AND (edges.child = lnwbum1.upper_milestone_id)
            )
        )
    )
    GROUP BY
    lnwbum1.student_id,
    lnwbum1.upper_milestone_id,
    lnwbum1.tree_id,
    edges.parent,
    student_total_children_weights.total_weights
    ORDER BY
    1,
    2,
    3;
    INSERT INTO eval.calculated_grades( student_id, milestone_id, tree_id, grade)
    SELECT student_id, milestone_id, tree_id, grade FROM eval.student_milestones_grades
    WHERE milestone_id NOT IN (SELECT DISTINCT id FROM eval.leafs_by_tree );

    INSERT INTO eval.inserted_grades( student_id, milestone_id, tree_id, grade)
    SELECT student_id, milestone_id, tree_id, grade FROM eval.student_milestones_grades
    WHERE milestone_id IN (SELECT DISTINCT id FROM eval.leafs_by_tree );



    DROP VIEW IF EXISTS eval.student_milestones_grades;
    DROP VIEW IF EXISTS eval.student_total_children_weights;
    DROP VIEW IF EXISTS eval.leafs_normalized_weight_by_upper_milestone;
    DROP VIEW IF EXISTS eval.student_milestones_normalized_weights_by_siblings;
    DROP VIEW IF EXISTS eval.leafs_by_tree;
    DROP VIEW IF EXISTS eval.actual_student_weight;
    DROP VIEW IF EXISTS eval.groups_eval_trees_by_ancestors;
    DROP VIEW IF EXISTS core.group_ancestors;
    DROP VIEW IF EXISTS core.users_roles_groups;
    DROP VIEW IF EXISTS core.students;

COMMIT;
