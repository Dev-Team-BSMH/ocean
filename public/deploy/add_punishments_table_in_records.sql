-- Deploy pacific-ocean:add_punishments_table_in_records to pg

BEGIN;

CREATE TABLE records.punishments (
    id serial,
    post_id integer NOT NULL,
    text text NOT NULL,
    CONSTRAINT records_punishments_pkey PRIMARY KEY (id),
    CONSTRAINT records_punishments_post_fkey FOREIGN KEY (post_id)
        REFERENCES records.posts (id)
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

COMMIT;
