-- Deploy pacific-ocean:add_purpose_for_post to pg

BEGIN;

    CREATE TABLE IF NOT EXISTS records.rank (
        id serial,
        numeric_value integer NOT NULL,
        "description" text NOT NULL,
        CONSTRAINT records_rank_pkey PRIMARY KEY (id)
    );

    CREATE TABLE IF NOT EXISTS records.goals (
        id serial,
        title text NOT NULL,
        content text NOT NULL,
        deadline date,
        rank integer NOT NULL,
        start_data date NOT NULL,
        accomplished boolean NOT NULL DEFAULT false,
        created_at timestamp with time zone NOT NULL DEFAULT now(),
        last_edited timestamp with time zone NOT NULL DEFAULT now(),
        cycle_id integer NOT NULL,
        CONSTRAINT record_goals_pkey PRIMARY KEY (id),
        CONSTRAINT record_goals_rank_id_fkey FOREIGN KEY (rank) REFERENCES records.rank (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
        CONSTRAINT record_goals_group_id_fkey FOREIGN KEY (cycle_id) REFERENCES core.groups (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
    );

    CREATE TABLE IF NOT EXISTS records.post_to_goal (
        id serial,
        goal_id integer NOT NULL,
        post_id integer NOT NULL,
        CONSTRAINT records_post_to_goal_pkey PRIMARY KEY (id),
        CONSTRAINT record_post_to_goal_goal_id_fkey FOREIGN KEY (goal_id) REFERENCES records.goals (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
        CONSTRAINT record_post_to_goal_post_id_fkey FOREIGN KEY (post_id) REFERENCES records.posts (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
    );

    ALTER TABLE records.post_task
        ADD COLUMN IF NOT EXISTS accomplished boolean NOT NULL DEFAULT false;

    ALTER TABLE records.posts
        RENAME instructor_content TO student_content;

    CREATE OR REPLACE FUNCTION update_last_edited_date_record_goals()
        RETURNS trigger AS
        $$
        BEGIN
            UPDATE records.goals
            SET last_updated = now()
            WHERE goals.id = NEW.id;
            RETURN NEW;
        END;
        $$
        LANGUAGE 'plpgsql';
    
    DROP TRIGGER IF EXISTS update_last_edited_date on records.goals;
    CREATE TRIGGER update_last_edited_date  
        AFTER UPDATE OF title, content, deadline, rank, start_data, accomplished, cycle_id
        ON records.goals
        FOR EACH ROW
        EXECUTE PROCEDURE update_last_edited_date();

COMMIT;
