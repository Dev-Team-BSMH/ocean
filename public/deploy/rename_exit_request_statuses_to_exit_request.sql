-- Deploy pacific-ocean:rename_exit_request_statuses_to_exit_request to pg

BEGIN;

ALTER TABLE records.exit_requests_statuses
    RENAME TO exit_requests;

COMMIT;
