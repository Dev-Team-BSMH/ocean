-- Deploy pacific-ocean:change_start_data_to_start_date_in_goals_table to pg

BEGIN;

ALTER TABLE records.goals
RENAME start_data to start_date;

COMMIT;
