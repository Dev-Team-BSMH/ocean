-- Deploy pacific-ocean:change_leaf_to_milestone_grades_table to pg
BEGIN;
ALTER TABLE eval.grades
  RENAME leaf_id TO milestone_id;
COMMIT;