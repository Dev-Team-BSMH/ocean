-- Deploy pacific-ocean:insert-data-for-old-exit-requests to pg

BEGIN;
    SET CLIENT_ENCODING TO 'utf8';

    INSERT INTO records.exit_requests("date", "status", post_id)
    SELECT 
        posts.event_date,
        'pending' AS status,
        posts.id
    FROM records.posts
    JOIN records.post_types ON post_types.id = posts.type_id
    WHERE post_types.type = 'בקשת יציאה';

COMMIT;
