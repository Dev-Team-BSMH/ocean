-- Deploy pacific-ocean:rename_content_columns_in_posts to pg

BEGIN;

    ALTER TABLE records.posts
        RENAME student_content TO instructor_content;

    ALTER TABLE records.posts
        RENAME content TO student_content;

COMMIT;
