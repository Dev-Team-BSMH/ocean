-- Deploy pacific-ocean:add_group_tag_table to pg

BEGIN;

    CREATE TABLE records.posts_groups_tags (
        id serial,
        post_id integer NOT NULL,
        group_id integer NOT NULL,
        CONSTRAINT posts_groups_tags_pk PRIMARY KEY (id)
    ); 

    ALTER TABLE records.posts_groups_tags
        ADD CONSTRAINT posts_groups_tags_posts_fkey FOREIGN KEY (post_id)
        REFERENCES records.posts (id) 
        ON UPDATE CASCADE
        ON DELETE CASCADE;

    ALTER TABLE records.posts_groups_tags
        ADD CONSTRAINT posts_groups_tags_groupss_fkey FOREIGN KEY (group_id)
        REFERENCES core.groups (id) 
        ON UPDATE CASCADE
        ON DELETE CASCADE;

COMMIT;
