-- Deploy pacific-ocean:add_enum_table_for_gender_type to pg

BEGIN;

    SET CLIENT_ENCODING TO 'utf8';

    CREATE TABLE core.genders (
        gender text NOT NULL,
        CONSTRAINT gender_pkey PRIMARY KEY (gender)
    );

    INSERT INTO core.genders(gender)
    VALUES ('זכר');

    INSERT INTO core.genders(gender)
    VALUES ('נקבה');

    ALTER TABLE core.users
        RENAME gender TO old_gender;

    ALTER TABLE core.users
        ADD COLUMN gender text DEFAULT 'זכר';

    ALTER TABLE core.users
        ADD FOREIGN KEY (gender)
        REFERENCES core.genders(gender)
        ON UPDATE CASCADE
        ON DELETE CASCADE;
        
    UPDATE core.users 
    SET gender =  old_gender;

    ALTER TABLE core.users DROP COLUMN old_gender CASCADE;

    ALTER TABLE core.users
        ALTER COLUMN gender SET NOT NULL;
    
    DROP TYPE core.gender;

COMMIT;
