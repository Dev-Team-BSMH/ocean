-- Deploy pacific-ocean:return_student_content_to_content to pg

BEGIN;

    ALTER TABLE records.posts
        RENAME student_content TO content;

COMMIT;
