-- Deploy pacific-ocean:add_special_pricing_table to pg
BEGIN;
CREATE TABLE eval.special_pricing (
  id serial,
  price integer NOT NULL,
  description text NOT NULL,
  CONSTRAINT special_pricing_pkey PRIMARY KEY (id)
);
CREATE TABLE eval.milestones_special_pricings (
  id serial,
  milestone_id integer NOT NULL,
  price_id integer NOT NULL,
  CONSTRAINT milestones_special_pricings_pkey PRIMARY KEY (id),
  CONSTRAINT milestones_special_pricings_milestone_id_fkey FOREIGN KEY (milestone_id) REFERENCES eval.milestones (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT milestones_special_pricings_price_id_fkey FOREIGN KEY (price_id) REFERENCES eval.special_pricing (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE eval.students_pricing (
  id serial,
  milestone_pricing_id integer NOT NULL,
  student_id integer NOT NULL,
  CONSTRAINT students_pricing_pkey PRIMARY KEY (id),
  CONSTRAINT pricing_and_student_unique UNIQUE (milestone_pricing_id, student_id),
  CONSTRAINT students_pricing_milestone_pricing_id_fkey FOREIGN KEY (milestone_pricing_id) REFERENCES eval.milestones_special_pricings (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT students_pricing_student_id_fkey FOREIGN KEY (student_id) REFERENCES core.users_roles (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
);
COMMIT;