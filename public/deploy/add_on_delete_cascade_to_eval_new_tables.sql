-- Deploy pacific-ocean:add_on_delete_cascade_to_eval_new_tables to pg

BEGIN;

ALTER TABLE eval.milestone_missings
    DROP CONSTRAINT milestone_missings_milestone_id_fkey;

ALTER TABLE eval.milestones_descriptions
    DROP CONSTRAINT milestones_descriptions_milestone_id_fkey;

ALTER TABLE eval.milestones_special_pricings
    DROP CONSTRAINT milestones_special_pricings_milestone_id_fkey;

ALTER TABLE eval.milestones_tags
    DROP CONSTRAINT milestones_tags_milestone_id_fkey;

ALTER TABLE eval.milestone_missings
    ADD FOREIGN KEY (milestone_id) REFERENCES eval.milestones(id) ON DELETE CASCADE;

ALTER TABLE eval.milestones_descriptions
    ADD FOREIGN KEY (milestone_id) REFERENCES eval.milestones(id) ON DELETE CASCADE;

ALTER TABLE eval.milestones_special_pricings
    ADD FOREIGN KEY (milestone_id) REFERENCES eval.milestones(id) ON DELETE CASCADE;

ALTER TABLE eval.milestones_tags
    ADD FOREIGN KEY (milestone_id) REFERENCES eval.milestones(id) ON DELETE CASCADE;

COMMIT;
