-- Deploy pacific-ocean:adding_instructor_content_column_to_records_posts to pg

BEGIN;

ALTER TABLE records.posts
    ADD COLUMN instructor_content TEXT;

COMMIT;
