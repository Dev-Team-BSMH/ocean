-- Deploy pacific-ocean:eval_edges_removal_and_fix_milestones_in_grades to pg

BEGIN;

ALTER TABLE eval.grades_descriptions
    ADD CONSTRAINT grades_descriptions_inserted_grades_fkey FOREIGN KEY (grade_id)
    REFERENCES eval.inserted_grades (id) 
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

DELETE 
FROM eval.milestones
WHERE id NOT IN (
	(SELECT DISTINCT child FROM eval.edges)
UNION ALL
(SELECT DISTINCT parent FROM eval.edges)
);

CREATE TABLE eval.new_milestones(
id SERIAL PRIMARY KEY,
name TEXT,
old_milestone INTEGER,
old_parent INTEGER,
parent INTEGER REFERENCES eval.new_milestones(id) ON DELETE CASCADE,
tree_id INTEGER,
weight REAL,
"order" INTEGER);

INSERT INTO eval.new_milestones(name, old_milestone, tree_id, weight, "order")
SELECT DISTINCT ON (tree_id)
	milestones.name,milestones.id, edges.tree_id, 100, edges.order
FROM eval.milestones
JOIN eval.edges ON milestones.id = edges.parent
WHERE milestones.id NOT IN (SELECT edges.child FROM eval.edges);

INSERT INTO eval.new_milestones(name, old_milestone, old_parent, tree_id, weight, "order")
SELECT name, milestones.id as old_milestone, parent as old_parent, tree_id, edges.weight, edges.order
FROM eval.milestones
JOIN eval.edges ON milestones.id = edges.child;

UPDATE eval.new_milestones
SET parent = parents.id
FROM eval.new_milestones as children
JOIN eval.new_milestones as parents ON children.old_parent = parents.old_milestone AND children.tree_id = parents.tree_id
WHERE new_milestones.old_milestone = children.old_milestone AND children.tree_id = new_milestones.tree_id;


CREATE TABLE eval.new_milestone_missings(
    id SERIAL PRIMARY KEY,
    milestone_id integer NOT NULL REFERENCES eval.milestones(id) ON DELETE CASCADE,
	new_milestone_id integer REFERENCES eval.new_milestones(id) ON DELETE CASCADE,
    student_id integer NOT NULL,
    reason text DEFAULT ''::text NOT NULL
);

INSERT INTO eval.new_milestone_missings(id, milestone_id, student_id, reason)
SELECT milestone_missings.id, milestone_missings.milestone_id, milestone_missings.student_id, milestone_missings.reason
FROM eval.milestone_missings;

UPDATE eval.new_milestone_missings
SET new_milestone_id = new_milestones.id
FROM eval.new_milestones
WHERE milestone_id = old_milestone;


CREATE TABLE eval.new_milestones_descriptions(
    id SERIAL PRIMARY KEY,
    milestone_id integer NOT NULL REFERENCES eval.milestones(id) ON DELETE CASCADE,
	new_milestone_id integer REFERENCES eval.new_milestones(id) ON DELETE CASCADE,
    description text DEFAULT ''::text NOT NULL,
    stars integer NOT NULL
);

INSERT INTO eval.new_milestones_descriptions(id, milestone_id, description, stars)
SELECT milestones_descriptions.id, milestones_descriptions.milestone_id, milestones_descriptions.description, milestones_descriptions.stars
FROM eval.milestones_descriptions;

UPDATE eval.new_milestones_descriptions
SET new_milestone_id = new_milestones.id
FROM eval.new_milestones
WHERE milestone_id = old_milestone;

CREATE TABLE eval.new_milestones_tags (
    id SERIAL PRIMARY KEY,
    milestone_id integer NOT NULL REFERENCES eval.milestones(id) ON DELETE CASCADE,
	new_milestone_id integer REFERENCES eval.new_milestones(id) ON DELETE CASCADE,
    tag_id integer NOT NULL REFERENCES eval.tags(id) ON DELETE CASCADE
);

INSERT INTO eval.new_milestones_tags(id, milestone_id, tag_id)
SELECT milestones_tags.id, milestones_tags.milestone_id, milestones_tags.tag_id
FROM eval.milestones_tags;

UPDATE eval.new_milestones_tags
SET new_milestone_id = new_milestones.id
FROM eval.new_milestones
WHERE milestone_id = old_milestone;

CREATE TABLE eval.new_scales (
    id SERIAL PRIMARY KEY,
    milestone_id integer NOT NULL REFERENCES eval.milestones(id) ON DELETE CASCADE,
	new_milestone_id integer REFERENCES eval.new_milestones(id) ON DELETE CASCADE,
    scale integer NOT NULL
);

INSERT INTO eval.new_scales(id, milestone_id, scale)
SELECT scales.id, scales.milestone_id, scales.scale
FROM eval.scales;

UPDATE eval.new_scales
SET new_milestone_id = new_milestones.id
FROM eval.new_milestones
WHERE milestone_id = old_milestone;

CREATE TABLE eval.new_special_pricing(
    id SERIAL PRIMARY KEY,
    milestone_id integer NOT NULL REFERENCES eval.milestones(id) ON DELETE CASCADE,
	new_milestone_id integer REFERENCES eval.new_milestones(id) ON DELETE CASCADE,
    price_id integer NOT NULL
);

INSERT INTO eval.new_special_pricing(id, milestone_id, price_id)
SELECT milestones_special_pricings.id, milestones_special_pricings.milestone_id, milestones_special_pricings.price_id
FROM eval.milestones_special_pricings;

UPDATE eval.new_special_pricing
SET new_milestone_id = new_milestones.id
FROM eval.new_milestones
WHERE milestone_id = old_milestone;

CREATE TABLE eval.group_eval_roots(
id SERIAL PRIMARY KEY,
group_id INTEGER REFERENCES core.groups(id) ON DELETE CASCADE,
root_id INTEGER REFERENCES eval.milestones(id) ON DELETE CASCADE,
tree_id INTEGER REFERENCES eval.trees(id) ON DELETE CASCADE,
new_root_id INTEGER REFERENCES eval.new_milestones(id) ON DELETE CASCADE
);

INSERT INTO eval.group_eval_roots(id, group_id, root_id, tree_id)
SELECT group_evaluation_trees.id, group_evaluation_trees.group_id, group_evaluation_trees.root_id, group_evaluation_trees.tree_id 
FROM eval.group_evaluation_trees;

UPDATE eval.group_eval_roots
SET new_root_id = new_milestones.id
FROM eval.new_milestones
WHERE old_milestone = root_id AND new_milestones.tree_id = group_eval_roots.tree_id;

DELETE FROM eval.group_eval_roots
	WHERE new_root_id is null;


DROP TABLE eval.milestones,
		   eval.milestones_descriptions,
		   eval.milestone_missings,
		   eval.milestones_tags,
		   eval.scales,
		   eval.milestones_special_pricings,
		   eval.group_evaluation_trees CASCADE;
		   
DELETE FROM eval.new_milestones_tags
	WHERE new_milestone_id is null;
		   
ALTER TABLE eval.new_milestones
    RENAME TO milestones;
ALTER TABLE eval.new_milestone_missings
    RENAME TO milestone_missings;
ALTER TABLE eval.new_milestones_descriptions
    RENAME TO milestones_descriptions;
ALTER TABLE eval.new_milestones_tags
    RENAME TO milestones_tags;
ALTER TABLE eval.new_scales
    RENAME TO scales;
ALTER TABLE eval.new_special_pricing
    RENAME TO milestones_special_pricings;

--ALTER TABLE eval.milestones DROP COLUMN old_milestone CASCADE;
--ALTER TABLE eval.milestones DROP COLUMN old_parent CASCADE;
--ALTER TABLE eval.milestones DROP COLUMN tree_id CASCADE;

ALTER TABLE eval.milestone_missings 
DROP CONSTRAINT new_milestone_missings_new_milestone_id_fkey;

ALTER TABLE eval.milestones_descriptions 
DROP CONSTRAINT new_milestones_descriptions_new_milestone_id_fkey;

ALTER TABLE eval.milestones_tags 
DROP CONSTRAINT new_milestones_tags_new_milestone_id_fkey;

ALTER TABLE eval.scales 
DROP CONSTRAINT new_scales_new_milestone_id_fkey;

ALTER TABLE eval.milestones_special_pricings 
DROP CONSTRAINT new_special_pricing_new_milestone_id_fkey;

UPDATE eval.milestone_missings
SET milestone_id = new_milestone_id;
ALTER TABLE eval.milestone_missings DROP COLUMN new_milestone_id CASCADE;

UPDATE eval.milestones_descriptions
SET milestone_id = new_milestone_id;
ALTER TABLE eval.milestones_descriptions DROP COLUMN new_milestone_id CASCADE;

UPDATE eval.milestones_tags
SET milestone_id = new_milestone_id;
ALTER TABLE eval.milestones_tags DROP COLUMN new_milestone_id CASCADE;

UPDATE eval.scales
SET milestone_id = new_milestone_id;
ALTER TABLE eval.scales DROP COLUMN new_milestone_id CASCADE;

UPDATE eval.milestones_special_pricings
SET milestone_id = new_milestone_id;
ALTER TABLE eval.milestones_special_pricings DROP COLUMN new_milestone_id CASCADE;

UPDATE eval.group_eval_roots
SET root_id = new_root_id;
ALTER TABLE eval.group_eval_roots DROP COLUMN new_root_id CASCADE;

ALTER TABLE eval.milestone_missings
    ADD FOREIGN KEY (milestone_id) REFERENCES eval.milestones(id);

ALTER TABLE eval.milestones_descriptions
    ADD FOREIGN KEY (milestone_id) REFERENCES eval.milestones(id);

ALTER TABLE eval.scales
    ADD FOREIGN KEY (milestone_id) REFERENCES eval.milestones(id);

ALTER TABLE eval.milestones_special_pricings
    ADD FOREIGN KEY (milestone_id) REFERENCES eval.milestones(id);

ALTER TABLE eval.milestones_tags
    ADD FOREIGN KEY (milestone_id) REFERENCES eval.milestones(id);

ALTER TABLE eval.milestone_missings ALTER COLUMN milestone_id SET NOT NULL;
ALTER TABLE eval.milestones_descriptions ALTER COLUMN milestone_id SET NOT NULL;
ALTER TABLE eval.milestones_tags ALTER COLUMN milestone_id SET NOT NULL;
ALTER TABLE eval.scales ALTER COLUMN milestone_id SET NOT NULL;
ALTER TABLE eval.milestones_special_pricings ALTER COLUMN milestone_id SET NOT NULL;
ALTER TABLE eval.milestones ALTER COLUMN weight SET NOT NULL;
ALTER TABLE eval.group_eval_roots ALTER COLUMN group_id SET NOT NULL;
ALTER TABLE eval.group_eval_roots ALTER COLUMN root_id SET NOT NULL;
ALTER TABLE eval.group_eval_roots ALTER COLUMN tree_id SET NOT NULL;

ALTER TABLE eval.milestones RENAME CONSTRAINT new_milestones_pkey TO milestones_pkey;
ALTER TABLE eval.milestones RENAME CONSTRAINT new_milestones_parent_fkey TO milestones_parent_fkey;
ALTER TABLE eval.milestone_missings RENAME CONSTRAINT new_milestone_missings_pkey TO milestone_missings_pkey;
ALTER TABLE eval.milestones_tags RENAME CONSTRAINT new_milestones_tags_pkey TO milestones_tags_pkey;
ALTER TABLE eval.milestones_tags RENAME CONSTRAINT new_milestones_tags_tag_id_fkey TO milestones_tags_tag_id_fkey;
ALTER TABLE eval.milestones_descriptions RENAME CONSTRAINT new_milestones_descriptions_pkey TO milestones_descriptions_pkey;
ALTER TABLE eval.scales RENAME CONSTRAINT new_scales_pkey TO scales_pkey;
ALTER TABLE eval.milestones_special_pricings RENAME CONSTRAINT new_special_pricing_pkey TO milestones_special_pricing_pkey;

ALTER TABLE eval.students_edges_weights
    RENAME TO students_milestones_weights;
ALTER TABLE eval.students_milestones_weights 
ADD COLUMN milestone_id integer NOT NULL REFERENCES eval.milestones(id);
ALTER TABLE eval.students_milestones_weights 
DROP CONSTRAINT students_edges_weights_student_id_fkey;
ALTER TABLE eval.students_milestones_weights RENAME CONSTRAINT students_edges_weights_pkey TO students_milestones_weights_pkey;
	
	
ALTER TABLE eval.milestones
    ADD COLUMN scale integer;

UPDATE eval.milestones
SET scale = 100;

UPDATE eval.milestones
SET scale = scales.scale
FROM eval.scales
WHERE scales.milestone_id = milestones.id;

DROP TABLE eval.scales;
ALTER TABLE eval.milestones
    ALTER COLUMN scale SET DEFAULT 100;

ALTER TABLE eval.milestones
    ALTER COLUMN scale SET NOT NULL;


ALTER TABLE eval.inserted_grades 
ADD COLUMN new_milestone_id INTEGER;

 ALTER TABLE eval.calculated_grades 
 ADD COLUMN new_milestone_id INTEGER;

 UPDATE eval.calculated_grades 
 SET new_milestone_id = milestones.id
 FROM eval.milestones
 WHERE milestones.old_milestone = calculated_grades.milestone_id 
 AND milestones.tree_id = calculated_grades.tree_id;

UPDATE eval.inserted_grades 
SET new_milestone_id = milestones.id
FROM eval.milestones
WHERE milestones.old_milestone = inserted_grades.milestone_id 
AND milestones.tree_id = inserted_grades.tree_id;

DELETE FROM eval.inserted_grades
	WHERE new_milestone_id is null;

DELETE FROM eval.calculated_grades
    WHERE new_milestone_id is null;

ALTER TABLE eval.inserted_grades
DROP CONSTRAINT inserted_grades_milestone_student_and_tree_unique;
ALTER TABLE eval.calculated_grades
DROP CONSTRAINT calculated_grades_milestone_student_and_tree_unique;

UPDATE eval.inserted_grades
SET milestone_id = new_milestone_id;
ALTER TABLE eval.inserted_grades DROP COLUMN new_milestone_id CASCADE;

UPDATE eval.calculated_grades
SET milestone_id = new_milestone_id;
ALTER TABLE eval.calculated_grades DROP COLUMN new_milestone_id CASCADE;

ALTER TABLE eval.calculated_grades 
   ADD CONSTRAINT calculated_grades_milestone_id_fkey
   FOREIGN KEY (milestone_id) 
   REFERENCES eval.milestones(id);
   
ALTER TABLE eval.inserted_grades 
   ADD CONSTRAINT inserted_grades_milestone_id_fkey
   FOREIGN KEY (milestone_id) 
   REFERENCES eval.milestones(id);
   
ALTER TABLE eval.group_eval_roots DROP COLUMN tree_id CASCADE;

ALTER TABLE eval.inserted_grades
ADD CONSTRAINT inserted_student_id_and_milestone_id_unique UNIQUE (student_id, milestone_id);

ALTER TABLE eval.milestones_special_pricings
ADD CONSTRAINT special_pricing_price_id_fkey FOREIGN KEY (price_id)
REFERENCES eval.special_pricing (id) MATCH SIMPLE
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE eval.calculated_grades
ADD CONSTRAINT calculated_student_id_and_milestone_id_unique UNIQUE (student_id, milestone_id);


ALTER TABLE eval.calculated_grades DROP COLUMN tree_id CASCADE;
ALTER TABLE eval.inserted_grades DROP COLUMN tree_id CASCADE;

COMMIT;
