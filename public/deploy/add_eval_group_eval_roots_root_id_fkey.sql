-- Deploy pacific-ocean:add_eval_group_eval_roots_root_id_fkey to pg

BEGIN;

ALTER TABLE eval.group_eval_roots
ADD CONSTRAINT group_eval_roots_root_id_fkey 
FOREIGN KEY (root_id) REFERENCES eval.milestones (id);

COMMIT;
