-- Deploy pacific-ocean:add_author_to_inserted_grades to pg

BEGIN;
    SET CLIENT_ENCODING TO 'utf8';
    CREATE OR REPLACE VIEW core.group_ancestors AS
        WITH RECURSIVE group_ancestors(group_id, ancestor_id) AS (
            SELECT groups.id AS group_id,
            edges.parent AS ancestor_id,
            1 AS depth
            FROM core.groups
                JOIN core.edges ON edges.child = groups.id
        UNION
            SELECT group_ancestors_1.group_id,
            edges.parent AS ancestor_id,
            group_ancestors_1.depth + 1 AS depth
            FROM core.groups
                JOIN group_ancestors group_ancestors_1 ON group_ancestors_1.group_id = groups.id
                JOIN core.edges ON edges.child = group_ancestors_1.ancestor_id
        )
        SELECT 
            group_ancestors.group_id,
            group_ancestors.ancestor_id,
            group_ancestors.depth
        FROM group_ancestors
        UNION
        SELECT groups.id AS group_id,
        groups.id AS ancestor_id,
        0 AS depth
        FROM core.groups
        ORDER BY 1, 2;

    CREATE OR REPLACE VIEW core.students AS 
        SELECT 
            users_roles.id,
            users_roles.user_id
        FROM core.users_roles
        JOIN core.roles ON roles.id = users_roles.role_id AND roles.role = 'חניך'::text;
    
    CREATE OR REPLACE VIEW core.group_for_soldier_id AS
        SELECT 
            users.soldier_id,
            group_ancestors.ancestor_id AS group_id
        FROM core.users_roles
        JOIN core.assigns ON assigns.user_role_id = users_roles.id
        JOIN core.users ON users_roles.user_id = users.id
        JOIN core.group_ancestors ON group_ancestors.group_id = assigns.group_id
        ORDER BY users.soldier_id, group_ancestors.ancestor_id;

    ALTER TABLE eval.inserted_grades
        ADD COLUMN author integer;
    ALTER TABLE eval.inserted_grades
        ADD FOREIGN KEY (author)
        REFERENCES core.users(soldier_id)
        ON UPDATE CASCADE
        ON DELETE CASCADE;

    CREATE OR REPLACE VIEW core.students_commanders AS 
        SELECT  students.id AS student_id,
                users2.soldier_id AS commander_id 	
            FROM core.students
            JOIN core.users ON users.id = students.user_id
            JOIN core.group_for_soldier_id ON group_for_soldier_id.soldier_id = users.soldier_id
            JOIN core."groups" ON "groups".id = group_for_soldier_id.group_id
            JOIN core.group_types ON group_types.id = "groups".group_type_id
            JOIN core.assigns ON "groups".id = assigns.group_id
            JOIN core.users_roles ON users_roles.id = assigns.user_role_id
            JOIN core.roles ON roles.id = users_roles.role_id
            JOIN core.users_roles users_roles2 ON users_roles2.id = assigns.user_role_id
            JOIN core.users users2 ON users2.id = users_roles2.user_id
            WHERE roles."role" != 'רמ"ד' AND roles."role" != 'חניך';
    
    DO $$
        DECLARE
            rec RECORD;
        BEGIN
            FOR rec IN 
                SELECT DISTINCT * from core.students_commanders
            LOOP 
                UPDATE eval.inserted_grades
                SET author = rec.commander_id
                WHERE inserted_grades.student_id = rec.student_id;
            END LOOP;
        END
    $$
    LANGUAGE plpgsql;


    ALTER TABLE eval.inserted_grades
        ALTER COLUMN author SET NOT NULL;

    DROP VIEW IF EXISTS core.students_commanders;
	DROP VIEW IF EXISTS core.group_ancestors CASCADE;
    DROP VIEW IF EXISTS core.students CASCADE;

    

COMMIT;
