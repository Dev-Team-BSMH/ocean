-- Deploy pacific-ocean:adding_insertion_data_to_students_pricing_and_grades to pg

BEGIN;

    ALTER TABLE eval.students_pricing
        ADD COLUMN created_at timestamp without time zone NOT NULL DEFAULT now();
    
    ALTER TABLE eval.students_pricing
        ADD COLUMN last_updated timestamp without time zone NOT NULL DEFAULT now();
    
    CREATE OR REPLACE FUNCTION update_last_edited_date()
        RETURNS trigger AS
        $$
        BEGIN
            UPDATE eval.students_pricing
            SET last_updated = now()
            WHERE students_pricing.id = NEW.id;
			RETURN NEW;
        END;
        $$
        LANGUAGE 'plpgsql';
    
    CREATE TRIGGER update_last_edited_date  
        AFTER UPDATE OF milestone_pricing_id,student_id
        ON eval.students_pricing
        FOR EACH ROW
        EXECUTE PROCEDURE update_last_edited_date();

COMMIT;
