-- Deploy pacific-ocean:add_table_for_exit_req_posts_status to pg

BEGIN;
    SET CLIENT_ENCODING TO 'utf8';

    CREATE TABLE records.requests_statuses (
        "status" text NOT NULL,
        CONSTRAINT requests_statuses_pkey PRIMARY KEY ("status")
    );

    INSERT INTO records.requests_statuses("status")
    VALUES ('בתהליך');

    INSERT INTO records.requests_statuses("status")
    VALUES ('אושרה');

    INSERT INTO records.requests_statuses("status")
    VALUES ('נדחתה');

    CREATE TABLE records.exit_requests_statuses (
        id serial,
        "date" date NOT NULL,
        "status" text NOT NULL,
        post_id integer NOT NULL,
        CONSTRAINT exit_requests_statuses_pkey PRIMARY KEY (id)
    );

    ALTER TABLE records.exit_requests_statuses
        ADD FOREIGN KEY (post_id)
        REFERENCES records.posts(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE;

    ALTER TABLE records.exit_requests_statuses
        ADD FOREIGN KEY ("status")
        REFERENCES records.requests_statuses("status")
        ON UPDATE CASCADE
        ON DELETE CASCADE;

COMMIT;
