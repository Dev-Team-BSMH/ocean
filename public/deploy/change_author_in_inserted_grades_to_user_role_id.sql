BEGIN;

ALTER TABLE eval.inserted_grades DROP CONSTRAINT IF EXISTS inserted_grades_author_fkey;

WITH soldier_id_for_instructor AS (
    SELECT DISTINCT author, users_roles.id as user_role
    FROM eval.inserted_grades
    JOIN core.users ON users.soldier_id = inserted_grades.author
    JOIN core.users_roles ON users_roles.user_id = users.id
    ORDER BY user_role
  )
UPDATE eval.inserted_grades
SET
  author = soldier_id_for_instructor.user_role
FROM soldier_id_for_instructor
WHERE
  soldier_id_for_instructor.author = inserted_grades.author;
ALTER TABLE eval.inserted_grades DROP CONSTRAINT inserted_student_id_and_milestone_id_unique;
ALTER TABLE eval.inserted_grades
ADD UNIQUE (student_id, milestone_id, author);
ALTER TABLE eval.inserted_grades RENAME author TO author_id;
ALTER TABLE eval.inserted_grades ADD FOREIGN KEY (author_id) REFERENCES core.users_roles (id) ON UPDATE CASCADE ON DELETE CASCADE;

COMMIT;