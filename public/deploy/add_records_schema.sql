-- Deploy pacific-ocean:add_records_schema to pg
BEGIN;
SET
  CLIENT_ENCODING TO 'utf8';
--
  -- Name: records; Type: SCHEMA; Schema: -; Owner: devteam
  --
  CREATE SCHEMA records;
--
  -- Name: post_visibility; Type: TYPE; Schema: public; Owner: devteam
  --
  CREATE TYPE records.post_visibility AS ENUM ('פורסם', 'לא פורסם');
--
  -- Name: post_types; Type: TABLE; Schema: records; Owner: devteam
  --
  CREATE TABLE records.post_types (
    id serial,
    type text NOT NULL,
    icon text NOT NULL,
    CONSTRAINT post_types_pkey PRIMARY KEY (id),
    CONSTRAINT records_post_types_type UNIQUE (type)
  );
--
  -- Name: posts; Type: TABLE; Schema: records; Owner: devteam
  --
  CREATE TABLE records.posts (
    id serial,
    publisher_id integer NOT NULL,
    type_id integer NOT NULL,
    visibility records.post_visibility NOT NULL DEFAULT 'לא פורסם' :: records.post_visibility,
    content text NOT NULL,
    created_at timestamp with time zone NOT NULL DEFAULT now(),
    last_edited timestamp with time zone NOT NULL DEFAULT now(),
    event_date date,
    group_id integer NOT NULL,
    CONSTRAINT posts_pkey PRIMARY KEY (id),
    CONSTRAINT posts_group_id_fkey FOREIGN KEY (group_id) REFERENCES core.groups (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT posts_publisher_id_fkey FOREIGN KEY (publisher_id) REFERENCES core.users_roles (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT posts_type_id_fkey FOREIGN KEY (type_id) REFERENCES records.post_types (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: comments; Type: TABLE; Schema: records; Owner: devteam
  --
  CREATE TABLE records.comments (
    id serial,
    post_id integer NOT NULL,
    publisher_id integer NOT NULL,
    created_at timestamp with time zone NOT NULL DEFAULT now(),
    last_edited timestamp with time zone NOT NULL DEFAULT now(),
    content text NOT NULL,
    CONSTRAINT comments_pkey PRIMARY KEY (id),
    CONSTRAINT comments_post_id_fkey FOREIGN KEY (post_id) REFERENCES records.posts (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT comments_publisher_id_fkey FOREIGN KEY (publisher_id) REFERENCES core.users_roles (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: discipline_types; Type: TABLE; Schema: records; Owner: devteam
  --
  CREATE TABLE records.discipline_types (
    id serial,
    type text NOT NULL,
    CONSTRAINT discipline_types_pkey PRIMARY KEY (id),
    CONSTRAINT records_discipline_types_type UNIQUE (type)
  );
--
  -- Name: likes; Type: TABLE; Schema: records; Owner: devteam
  --
  CREATE TABLE records.likes (
    id serial,
    post_id integer NOT NULL,
    publisher_id integer NOT NULL,
    created_at timestamp with time zone NOT NULL DEFAULT now(),
    last_edited timestamp with time zone NOT NULL DEFAULT now(),
    CONSTRAINT likes_pkey PRIMARY KEY (id),
    CONSTRAINT likes_post_id_fkey FOREIGN KEY (post_id) REFERENCES records.posts (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT likes_publisher_id_fkey FOREIGN KEY (publisher_id) REFERENCES core.users_roles (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: post_discipline; Type: TABLE; Schema: records; Owner: devteam
  --
  CREATE TABLE records.post_discipline (
    id serial,
    post_id integer NOT NULL,
    discipline_id integer NOT NULL,
    CONSTRAINT post_discipline_pkey PRIMARY KEY (id),
    CONSTRAINT records_post_discipline_post_id_and_discipline_id UNIQUE (post_id, discipline_id),
    CONSTRAINT post_discipline_discipline_id_fkey FOREIGN KEY (discipline_id) REFERENCES records.discipline_types (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT post_discipline_post_id_fkey FOREIGN KEY (post_id) REFERENCES records.posts (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: post_task; Type: TABLE; Schema: records; Owner: devteam
  --
  CREATE TABLE records.post_task (
    id serial,
    post_id integer NOT NULL,
    task_deadline date NOT NULL,
    CONSTRAINT post_task_pkey PRIMARY KEY (id),
    CONSTRAINT post_task_post_id_fkey FOREIGN KEY (post_id) REFERENCES records.posts (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: post_visibility_for_users; Type: TABLE; Schema: records; Owner: devteam
  --
  CREATE TABLE records.post_visibility_for_users (
    id serial,
    post_id integer NOT NULL,
    user_role_id integer NOT NULL,
    CONSTRAINT post_visibility_for_users_pkey PRIMARY KEY (id),
    CONSTRAINT post_visibility_for_users_post_id_fkey FOREIGN KEY (post_id) REFERENCES records.posts (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT post_visibility_for_users_user_role_id_fkey FOREIGN KEY (user_role_id) REFERENCES core.users_roles (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: tags; Type: TABLE; Schema: records; Owner: devteam
  --
  CREATE TABLE records.tags (
    id serial,
    post_id integer NOT NULL,
    student_id integer NOT NULL,
    CONSTRAINT tags_pkey PRIMARY KEY (id),
    CONSTRAINT records_tags_student_id_and_post_id UNIQUE (student_id, post_id),
    CONSTRAINT tags_post_id_fkey FOREIGN KEY (post_id) REFERENCES records.posts (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT tags_student_id_fkey FOREIGN KEY (student_id) REFERENCES core.users_roles (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: post_types_access; Type: TABLE; Schema: records; Owner: devteam
  --
  CREATE TABLE records.post_types_access (
    id serial,
    post_type_id integer NOT NULL,
    write_access_role integer NOT NULL,
    CONSTRAINT post_types_access_pkey PRIMARY KEY (id),
    CONSTRAINT post_types_access_post_type_id_fkey FOREIGN KEY (post_type_id) REFERENCES records.post_types (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT post_types_access_write_access_role_fkey FOREIGN KEY (write_access_role) REFERENCES core.roles (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
  );
COMMIT;