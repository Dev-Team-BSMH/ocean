-- Deploy pacific-ocean:fix_sequence_problem_in_eval_schema to pg

BEGIN;

SELECT setval('eval.group_eval_roots_id_seq',coalesce(max(id),1),true)
FROM eval.group_eval_roots;

ALTER SEQUENCE eval.new_milestone_missings_id_seq
    RENAME TO milestone_missings_id_seq;

SELECT setval('eval.milestone_missings_id_seq',coalesce(max(id),1),true)
FROM eval.milestone_missings;

ALTER SEQUENCE eval.new_milestones_descriptions_id_seq
    RENAME TO milestones_descriptions_id_seq;

SELECT setval('eval.milestones_descriptions_id_seq',coalesce(max(id),1),true)
FROM eval.milestones_descriptions;

ALTER SEQUENCE eval.new_milestones_tags_id_seq
    RENAME TO milestones_tags_id_seq;

SELECT setval('eval.milestones_tags_id_seq',coalesce(max(id),1),true)
FROM eval.milestones_tags;

ALTER SEQUENCE eval.new_special_pricing_id_seq
    RENAME TO milestones_special_pricing_id_seq;

SELECT setval('eval.milestones_special_pricing_id_seq',coalesce(max(id),1),true)
FROM eval.milestones_special_pricings;

ALTER SEQUENCE eval.new_milestones_id_seq
    RENAME TO milestones_id_seq;

COMMIT;
