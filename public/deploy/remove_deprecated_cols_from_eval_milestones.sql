-- Deploy pacific-ocean:remove_deprecated_cols_from_eval_milestones to pg

BEGIN;

ALTER TABLE eval.milestones
DROP COLUMN old_milestone CASCADE, DROP COLUMN old_parent CASCADE, DROP COLUMN tree_id CASCADE;

COMMIT;
