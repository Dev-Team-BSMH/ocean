-- Deploy pacific-ocean:transfer_posts_group_ids_to_cycle to pg

BEGIN;
    SET CLIENT_ENCODING TO 'utf8';

    CREATE OR REPLACE VIEW core.group_ancestors AS
        WITH RECURSIVE group_ancestors(group_id, ancestor_id) AS (
        SELECT
            groups.id AS group_id,
            edges.parent AS ancestor_id,
            1 AS depth
        FROM core.groups
        JOIN core.edges ON edges.child = groups.id
        UNION
        SELECT
            group_ancestors_1.group_id,
            edges.parent AS ancestor_id,
            group_ancestors_1.depth + 1 AS depth
        FROM core.groups
        JOIN group_ancestors group_ancestors_1 ON group_ancestors_1.group_id = groups.id
        JOIN core.edges ON edges.child = group_ancestors_1.ancestor_id)
        SELECT
            group_ancestors.group_id,
            group_ancestors.ancestor_id,
            group_ancestors.depth
        FROM group_ancestors
        UNION
        SELECT 
            groups.id AS group_id,
            groups.id AS ancestor_id,
        0 AS depth
        FROM core.groups
        ORDER BY 1, 2;

    WITH groups_and_cycles AS (
        SELECT 
            posts.group_id, 
            group_ancestors.ancestor_id 
        FROM records.posts
        JOIN core.group_ancestors ON group_ancestors.group_id = posts.group_id
        JOIN core.groups ON groups.id = group_ancestors.ancestor_id
        JOIN core.group_types ON group_types.id = "groups".group_type_id
        WHERE group_types."type" = 'מחזור'   
    )
        
    UPDATE records.posts
    SET group_id = groups_and_cycles.ancestor_id
    FROM groups_and_cycles
    WHERE posts.group_id = groups_and_cycles.group_id;

    DROP VIEW IF EXISTS core.group_ancestors CASCADE;

    CREATE TABLE records.templates (
        id serial,
        template text NOT NULL,
        post_type integer NOT NULL,
        CONSTRAINT templates_pkey PRIMARY KEY (id),
        CONSTRAINT records_templates_post_type_id_fkey FOREIGN KEY (post_type) 
            REFERENCES records.post_types (id) 
            MATCH SIMPLE 
            ON UPDATE CASCADE 
            ON DELETE CASCADE
    );

COMMIT;
