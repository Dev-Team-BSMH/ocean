-- Deploy pacific-ocean:add_a_relationship_between_goals_table_and_student to pg

BEGIN;

ALTER TABLE records.goals
ADD COLUMN student_user_role_id integer NOT NULL;

ALTER TABLE records.goals
ADD FOREIGN KEY (student_user_role_id)
REFERENCES core.users_roles(id)
ON UPDATE CASCADE
ON DELETE CASCADE;

COMMIT;
