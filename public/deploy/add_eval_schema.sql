-- Deploy pacific-ocean:create_eval_schema to pg
BEGIN;
SET
  CLIENT_ENCODING TO 'utf8';
--
  -- Name: eval; Type: SCHEMA; Schema: -; Owner: devteam
  --
  CREATE SCHEMA eval;
--
  -- Name: milestones; Type: TABLE; Schema: eval; Owner: devteam
  --
  CREATE TABLE eval.milestones (
    id serial,
    name text NOT NULL,
    CONSTRAINT milestones_pkey PRIMARY KEY (id)
  );
--
  -- Name: trees; Type: TABLE; Schema: eval; Owner: devteam
  --
  CREATE TABLE eval.trees (
    id serial,
    name text NOT NULL,
    CONSTRAINT trees_pkey PRIMARY KEY (id)
  );
--
  -- Name: group_evaluation_trees; Type: TABLE; Schema: eval; Owner: devteam
  --
  CREATE TABLE eval.group_evaluation_trees (
    id serial,
    root_id integer NOT NULL,
    group_id integer NOT NULL,
    tree_id serial,
    CONSTRAINT group_evaluation_trees_pkey PRIMARY KEY (id),
    CONSTRAINT eval_group_evaluation_trees_root_id_and_group_id_and_tree_id UNIQUE (root_id, group_id, tree_id),
    CONSTRAINT group_evaluation_trees_group_id_fkey FOREIGN KEY (group_id) REFERENCES core.groups (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT group_evaluation_trees_root_id_fkey FOREIGN KEY (root_id) REFERENCES eval.milestones (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT group_evaluation_trees_tree_id_fkey FOREIGN KEY (tree_id) REFERENCES eval.trees (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: edges; Type: TABLE; Schema: eval; Owner: devteam
  --
  CREATE TABLE eval.edges (
    id serial,
    child integer NOT NULL,
    parent integer NOT NULL,
    "order" integer NOT NULL,
    weight real NOT NULL,
    tree_id integer NOT NULL,
    CONSTRAINT edges_pkey PRIMARY KEY (id),
    CONSTRAINT eval_edges_child_and_parent_and_tree_id UNIQUE (child, parent, tree_id),
    CONSTRAINT edges_child_fkey FOREIGN KEY (child) REFERENCES eval.milestones (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT edges_parent_fkey FOREIGN KEY (parent) REFERENCES eval.milestones (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT edges_tree_id_fkey FOREIGN KEY (tree_id) REFERENCES eval.trees (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: students_edges_weights; Type: TABLE; Schema: eval; Owner: devteam
  --
  CREATE TABLE eval.students_edges_weights (
    id serial,
    edge_id integer NOT NULL,
    student_id integer NOT NULL,
    weight real NOT NULL,
    CONSTRAINT students_edges_weights_pkey PRIMARY KEY (id),
    CONSTRAINT eval_students_edges_weights_edge_id_and_student_id UNIQUE (edge_id, student_id),
    CONSTRAINT students_edges_weights_edge_id_fkey FOREIGN KEY (edge_id) REFERENCES eval.edges (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT students_edges_weights_student_id_fkey FOREIGN KEY (student_id) REFERENCES core.users_roles (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: milestones_descriptions; Type: TABLE; Schema: eval; Owner: devteam
  --
  CREATE TABLE eval.milestones_descriptions (
    id serial,
    milestone_id integer NOT NULL,
    description text NOT NULL,
    stars integer NOT NULL,
    CONSTRAINT description_pkey PRIMARY KEY (id),
    CONSTRAINT milestones_descriptions_milestone_id_fkey FOREIGN KEY (milestone_id) REFERENCES eval.milestones (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
  );
--
  -- Name: grades; Type: TABLE; Schema: eval; Owner: devteam
  --
  CREATE TABLE eval.grades (
    id serial,
    grade integer NOT NULL,
    leaf_id integer NOT NULL,
    student_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    CONSTRAINT grades_pkey PRIMARY KEY (id),
    CONSTRAINT eval_grades_leaf_id_and_student_id UNIQUE (leaf_id, student_id),
    CONSTRAINT grades_leaf_id_fkey FOREIGN KEY (leaf_id) REFERENCES eval.milestones (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT grades_student_id_fkey FOREIGN KEY (student_id) REFERENCES core.users_roles (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: scales; Type: TABLE; Schema: eval; Owner: devteam
  --
  CREATE TABLE eval.scales (
    id serial,
    milestone_id integer NOT NULL,
    scale integer NOT NULL,
    CONSTRAINT scales_pkey PRIMARY KEY (id),
    CONSTRAINT unique_milestone_id UNIQUE (milestone_id),
    CONSTRAINT scales_milestone_id_fkey FOREIGN KEY (milestone_id) REFERENCES eval.milestones (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: grades_descriptions; Type: TABLE; Schema: eval; Owner: devteam
  --
  CREATE TABLE eval.grades_descriptions (
    id serial,
    grade_id integer NOT NULL,
    description text NOT NULL,
    author_id integer NOT NULL,
    CONSTRAINT grades_descriptions_pkey PRIMARY KEY (id),
    CONSTRAINT grades_descriptions_grade_id_key UNIQUE (grade_id),
    CONSTRAINT grades_descriptions_author_id_fkey FOREIGN KEY (author_id) REFERENCES core.users_roles (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT grades_descriptions_grade_id_fkey FOREIGN KEY (grade_id) REFERENCES eval.grades (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: milestone_missings; Type: TABLE; Schema: eval; Owner: devteam
  --
  CREATE TABLE eval.milestone_missings (
    id serial,
    milestone_id integer NOT NULL,
    student_id integer NOT NULL,
    reason text NOT NULL DEFAULT '' :: text,
    CONSTRAINT milestone_missings_pkey PRIMARY KEY (id),
    CONSTRAINT eval_milestone_missings_milestone_id_and_student_id UNIQUE (milestone_id, student_id),
    CONSTRAINT milestone_missings_milestone_id_fkey FOREIGN KEY (milestone_id) REFERENCES eval.milestones (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT milestone_missings_student_id_fkey FOREIGN KEY (student_id) REFERENCES core.users_roles (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: tags; Type: TABLE; Schema: eval; Owner: devteam
  --
  CREATE TABLE eval.tags (
    id serial,
    tag text NOT NULL,
    CONSTRAINT tags_pkey PRIMARY KEY (id),
    CONSTRAINT eval_tags_tag UNIQUE (tag)
  );
--
  -- Name: milestones_tags; Type: TABLE; Schema: eval; Owner: devteam
  --
  CREATE TABLE eval.milestones_tags (
    id serial,
    milestone_id integer NOT NULL,
    tag_id integer NOT NULL,
    CONSTRAINT milestones_tags_pkey PRIMARY KEY (id),
    CONSTRAINT eval_milestones_tags_tag_id_and_milestone_id UNIQUE (milestone_id, tag_id),
    CONSTRAINT milestones_tags_milestone_id_fkey FOREIGN KEY (milestone_id) REFERENCES eval.milestones (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT milestones_tags_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES eval.tags (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
COMMIT;