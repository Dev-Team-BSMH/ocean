-- Deploy pacific-ocean:add_eval_self_evaluation_and_milestone_feedback_tables to pg

BEGIN;

  CREATE TABLE eval.milestone_feedback (
    id serial,
    instructor_id integer NOT NULL,
    milestone_id integer NOT NULL,
    feedback text NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    CONSTRAINT milestone_feedback_pkey PRIMARY KEY (id),
    CONSTRAINT milestone_feedback_instructor_fkey FOREIGN KEY (instructor_id)
        REFERENCES core.users_roles (id)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT milestone_feedback_milestone_fkey FOREIGN KEY (milestone_id)
        REFERENCES eval.milestones (id)
        ON UPDATE CASCADE
        ON DELETE CASCADE
  );
    CREATE TABLE eval.self_evaluation (
    id serial,
    student_id integer NOT NULL,
    milestone_id integer NOT NULL,
    grade double precision NOT NULL,
    description text NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    CONSTRAINT self_evaluation_pkey PRIMARY KEY (id),
    CONSTRAINT self_evaluation_student_fkey FOREIGN KEY (student_id)
        REFERENCES core.users_roles (id)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT self_evaluation_milestone_fkey FOREIGN KEY (milestone_id)
        REFERENCES eval.milestones (id)
        ON UPDATE CASCADE
        ON DELETE CASCADE
  );

    ALTER TABLE eval.grades_descriptions DROP COLUMN author_id CASCADE;

COMMIT;
