-- Deploy pacific-ocean:change_grade_type_to_real to pg
BEGIN;
ALTER TABLE eval.grades
ALTER COLUMN
  grade TYPE real;
COMMIT;