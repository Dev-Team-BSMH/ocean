-- Deploy pacific-ocean:create_mantle_schema_and_course_mador_shortcuts_tables to pg

BEGIN;

CREATE SCHEMA mantle;

CREATE TABLE mantle.course_shortcuts (
    id serial,
    course_id integer NOT NULL,
    shortcut text NOT NULL,
    CONSTRAINT course_shortcuts_pkey PRIMARY KEY (id),
    CONSTRAINT course_id_fkey FOREIGN KEY (course_id) REFERENCES core.groups (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
CREATE TABLE mantle.mador_shortcuts (
    id serial,
    mador_id integer NOT NULL,
    shortcut text NOT NULL,
    CONSTRAINT mador_id_shortcuts_pkey PRIMARY KEY (id),
    CONSTRAINT mador_id_fkey FOREIGN KEY (mador_id) REFERENCES core.groups (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );

COMMIT;
