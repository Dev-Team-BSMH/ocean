-- Deploy pacific-ocean:create_core_schema to pg
BEGIN;
SET
  CLIENT_ENCODING TO 'utf8';
--
  -- Name: core; Type: SCHEMA; Schema: -;
  --
  CREATE SCHEMA core;
--
  -- Name: gender; Type: TYPE; Schema: public; Owner: devteam
  --
  CREATE TYPE core.gender AS ENUM ('זכר', 'נקבה');
--
  -- Name: roles; Type: TABLE; Schema: core
  --
  CREATE TABLE core.roles (
    id serial,
    role text NOT NULL,
    CONSTRAINT roles_pkey PRIMARY KEY (id),
    CONSTRAINT core_roles_role_unique UNIQUE (role)
  );
--
  -- Name: users; Type: TABLE; Schema: core
  --
  CREATE TABLE core.users (
    id serial,
    soldier_id integer NOT NULL,
    first_name text NOT NULL,
    last_name text NOT NULL,
    gender core.gender NOT NULL DEFAULT 'זכר' :: core.gender,
    CONSTRAINT users_pkey PRIMARY KEY (id),
    CONSTRAINT core_users_soldier_id_unique UNIQUE (soldier_id)
  );
--
  -- Name: users_roles; Type: TABLE; Schema: core
  --
  CREATE TABLE core.users_roles (
    id serial,
    user_id integer NOT NULL,
    role_id integer NOT NULL,
    details json NOT NULL DEFAULT '{"details": []}' :: json,
    CONSTRAINT users_roles_pkey PRIMARY KEY (id),
    CONSTRAINT users_roles_role_id_fkey FOREIGN KEY (role_id) REFERENCES core.roles (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT users_roles_user_id_fkey FOREIGN KEY (user_id) REFERENCES core.users (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: users_roles_termination; Type: TABLE; Schema: core
  --
  CREATE TABLE core.users_roles_termination (
    id serial,
    user_role_id integer NOT NULL,
    reason text,
    CONSTRAINT users_roles_termination_pkey PRIMARY KEY (id),
    CONSTRAINT core_users_roles_termination_student_id_unique UNIQUE (user_role_id),
    CONSTRAINT users_roles_termination_user_role_id_fkey FOREIGN KEY (user_role_id) REFERENCES core.users_roles (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: group_types; Type: TABLE; Schema: core
  --
  CREATE TABLE core.group_types (
    id serial,
    type text NOT NULL,
    CONSTRAINT group_types_pkey PRIMARY KEY (id),
    CONSTRAINT core_group_types_type_unique UNIQUE (type)
  );
--
  -- Name: groups; Type: TABLE; Schema: core
  --
  CREATE TABLE core.groups (
    id serial,
    group_type_id integer NOT NULL,
    name text NOT NULL,
    CONSTRAINT groups_pkey PRIMARY KEY (id),
    CONSTRAINT groups_group_type_id_fkey FOREIGN KEY (group_type_id) REFERENCES core.group_types (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: groups_period; Type: TABLE; Schema: core
  --
  CREATE TABLE core.groups_period (
    id serial,
    group_id integer NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    CONSTRAINT groups_period_pkey PRIMARY KEY (id),
    CONSTRAINT core_groups_period_group_id__unique UNIQUE (group_id),
    CONSTRAINT groups_period_group_id_fkey FOREIGN KEY (group_id) REFERENCES core.groups (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: edges; Type: TABLE; Schema: core
  --
  CREATE TABLE core.edges (
    id serial,
    child integer NOT NULL,
    parent integer NOT NULL,
    CONSTRAINT edges_pkey PRIMARY KEY (id),
    CONSTRAINT edges_child_key UNIQUE (child),
    CONSTRAINT edges_child_fkey FOREIGN KEY (child) REFERENCES core.groups (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT edges_parent_fkey FOREIGN KEY (parent) REFERENCES core.groups (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
--
  -- Name: assigns; Type: TABLE; Schema: core
  --
  CREATE TABLE core.assigns (
    id serial,
    user_role_id integer NOT NULL,
    group_id integer NOT NULL,
    CONSTRAINT assigns_pkey PRIMARY KEY (id),
    CONSTRAINT core_assigns_users_roles_id_and_group_id_unique UNIQUE (user_role_id, group_id),
    CONSTRAINT assigns_group_id_fkey FOREIGN KEY (group_id) REFERENCES core.groups (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT assigns_user_role_id_fkey FOREIGN KEY (user_role_id) REFERENCES core.users_roles (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
  );
COMMIT;