-- Verify pacific-ocean:adding_instructor_content_column_to_records_posts on pg

BEGIN;

SELECT(
    EXISTS(
      SELECT
        *
      from INFORMATION_SCHEMA.columns
      where
        table_schema = 'records' and table_name = 'posts' and column_name = 'instructor_content'
    )
  );

ROLLBACK;
