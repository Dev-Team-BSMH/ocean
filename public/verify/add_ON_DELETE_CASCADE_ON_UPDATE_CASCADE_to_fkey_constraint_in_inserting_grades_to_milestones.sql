-- Verify pacific-ocean:add_ON_DELETE_CASCADE_ON_UPDATE_CASCADE_to_fkey_constraint_in_inserting_grades_to_milestones on pg

BEGIN;

SELECT(
    EXISTS(
        SELECT *
        FROM information_schema.referential_constraints
        WHERE constraint_schema='eval' and constraint_name='inserted_grades_milestone_id_fkey' and update_rule='CASCADE' and delete_rule='CASCADE'));
        
ROLLBACK;
