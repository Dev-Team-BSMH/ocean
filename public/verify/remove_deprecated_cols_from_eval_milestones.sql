-- Verify pacific-ocean:remove_deprecated_cols_from_eval_milestones on pg

BEGIN;
SELECT(
          NOT EXISTS
          (
                  SELECT *
                  FROM information_schema.columns
                  WHERE table_schema='eval' and table_name='mielstones' and
                  (column_name='old_parent' or column_name='old_milestones' or column_name='tree_id')
          )
      );

ROLLBACK;
