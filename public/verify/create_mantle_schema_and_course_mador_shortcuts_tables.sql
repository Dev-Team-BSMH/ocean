-- Verify pacific-ocean:create_mantle_schema_and_course_mador_shortcuts_tables on pg

BEGIN;

BEGIN;
SELECT(
    EXISTS(
      SELECT
        *
      from INFORMATION_SCHEMA.schemata
      where
        schema_name = 'mantle'
    )
    AND EXISTS(
      SELECT
        *
      FROM information_schema.check_constraints
      WHERE
        constraint_schema = 'mantle'
        and constraint_name = 'check_if_course'
        and check_clause = '(mantle.check_if_course(course_id))'
    )
    AND EXISTS(
      SELECT
        *
      FROM information_schema.check_constraints
      WHERE
        constraint_schema = 'mantle'
        and constraint_name = 'check_if_mador'
        and check_clause = '(mantle.check_if_mador(mador_id))'
    )
    AND EXISTS(
      SELECT
        *
      FROM information_schema.tables
      WHERE
        table_schema = 'mantle'
        and table_name = 'course_shortcuts'
    )
    AND EXISTS(
      SELECT
        *
      FROM information_schema.tables
      WHERE
        table_schema = 'mantle'
        and table_name = 'mador_shortcuts'
    )
  );
ROLLBACK;
