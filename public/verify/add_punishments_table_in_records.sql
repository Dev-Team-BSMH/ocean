-- Verify pacific-ocean:add_punishments_table_in_records on pg

BEGIN;

SELECT(
    EXISTS(
        SELECT *
        FROM information_schema.tables
        WHERE table_schema='records' and table_name='punishments'));
 

ROLLBACK;
