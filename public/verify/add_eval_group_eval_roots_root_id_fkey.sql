-- Verify pacific-ocean:add_eval_group_eval_roots_root_id_fkey on pg

BEGIN;

SELECT(
    EXISTS(
        SELECT *
        FROM information_schema.referential_constraints
        WHERE constraint_schema='eval' and constraint_name='group_eval_roots_root_id_fkey'));

ROLLBACK;
