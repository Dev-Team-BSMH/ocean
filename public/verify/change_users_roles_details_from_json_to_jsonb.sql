-- Verify pacific-ocean:change_users_roles_details_from_json_to_jsonb on pg

BEGIN;

SELECT(
    EXISTS(
      SELECT
        *
      from INFORMATION_SCHEMA.columns
      where
        table_schema = 'core' and table_name = 'users_roles' and column_name = 'details' and data_type = 'jsonb'
    )
)

ROLLBACK;
