-- Verify pacific-ocean:change_start_data_to_start_date_in_goals_table on pg

BEGIN;

SELECT(
    EXISTS(
      SELECT
        *
      from INFORMATION_SCHEMA.columns
      where
        table_schema = 'records' and table_name = 'goals' and column_name = 'start_date'
    )
  );

ROLLBACK;
