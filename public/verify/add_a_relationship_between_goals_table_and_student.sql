-- Verify pacific-ocean:add_a_relationship_between_goals_table_and_student on pg

BEGIN;

SELECT(
    EXISTS(
      SELECT
        *
      from INFORMATION_SCHEMA.columns
      where
        table_schema = 'records' and table_name = 'goals' and column_name = 'student_user_role_id'
    )
    AND EXISTS(
      SELECT
        *
      FROM information_schema.referential_constraints
      WHERE
        constraint_schema = 'records'
        and constraint_name = 'goals_student_user_role_id_fkey'
    )
  );

ROLLBACK;
