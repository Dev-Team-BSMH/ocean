-- Verify pacific-ocean:add_eval_self_evaluation_and_milestone_feedback_tables on pg

BEGIN;


SELECT(
    EXISTS(
        SELECT *
        FROM information_schema.tables
        WHERE table_schema='eval' and table_name='self_evaluation')
    AND EXISTS(
                SELECT *
                FROM information_schema.tables
                WHERE table_schema='eval' and table_name='milestone_feedback')
        AND NOT EXISTS(
            SELECT *
            FROM INFORMATION_SCHEMA.columns
            WHERE table_schema = 'eval' and table_name = 'grades_descriptions' and column_name = 'author_id')
        );

ROLLBACK;
