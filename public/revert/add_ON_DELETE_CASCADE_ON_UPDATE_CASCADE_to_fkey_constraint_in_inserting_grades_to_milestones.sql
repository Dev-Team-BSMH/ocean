-- Revert pacific-ocean:add_ON_DELETE_CASCADE_ON_UPDATE_CASCADE_to_fkey_constraint_in_inserting_grades_to_milestones from pg

BEGIN;

ALTER TABLE eval.inserted_grades
DROP CONSTRAINT inserted_grades_milestone_id_fkey;

ALTER TABLE eval.inserted_grades
ADD CONSTRAINT inserted_grades_milestone_id_fkey
FOREIGN KEY (milestone_id) 
REFERENCES eval.milestones(id);

COMMIT;