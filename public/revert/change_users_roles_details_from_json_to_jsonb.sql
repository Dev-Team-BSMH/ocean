-- Revert pacific-ocean:change_users_roles_details_from_json_to_jsonb from pg

BEGIN;

ALTER TABLE core.users_roles
ALTER COLUMN details
SET DATA TYPE json;

COMMIT;
