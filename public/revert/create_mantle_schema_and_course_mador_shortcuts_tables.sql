-- Revert pacific-ocean:create_mantle_schema_and_course_mador_shortcuts_tables from pg
BEGIN;

DROP TABLE IF EXISTS mantle.course_shortcuts;
DROP TABLE IF EXISTS mantle.mador_shortcuts;
DROP SCHEMA mantle; 

COMMIT;
