-- Revert pacific-ocean:add_eval_self_evaluation_and_milestone_feedback_tables from pg

BEGIN;

DROP TABLE IF EXISTS eval.milestone_feedback;
DROP TABLE IF EXISTS eval.self_evaluation;

ALTER TABLE eval.grades_descriptions
ADD COLUMN author_id integer;

UPDATE eval.grades_descriptions
SET author_id = (
    SELECT inserted_grades.author_id
    FROM eval.inserted_grades
    WHERE inserted_grades.id=grades_descriptions.grade_id);

ALTER TABLE eval.grades_descriptions
ADD CONSTRAINT grades_descriptions_author_id_fkey
FOREIGN KEY (author_id)
REFERENCES core.users_roles(id)
ON DELETE CASCADE ON UPDATE CASCADE;

COMMIT;
