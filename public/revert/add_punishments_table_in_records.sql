-- Revert pacific-ocean:add_punishments_table_in_records from pg

BEGIN;

DROP TABLE records.punishments CASCADE;

COMMIT;
