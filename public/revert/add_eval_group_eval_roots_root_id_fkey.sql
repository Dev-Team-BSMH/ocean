-- Revert pacific-ocean:add_eval_group_eval_roots_root_id_fkey from pg

BEGIN;

ALTER TABLE eval.group_eval_roots
DROP CONSTRAINT group_eval_roots_root_id_fkey;

COMMIT;
