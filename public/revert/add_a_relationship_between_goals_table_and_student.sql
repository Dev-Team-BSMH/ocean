-- Revert pacific-ocean:add_a_relationship_between_goals_table_and_student from pg

BEGIN;

ALTER TABLE records.goals
DROP COLUMN student_user_role_id CASCADE;

COMMIT;
