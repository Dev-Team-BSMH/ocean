-- Revert pacific-ocean:adding_instructor_content_column_to_records_posts from pg

BEGIN;

ALTER TABLE records.posts
    DROP COLUMN instructor_content;

COMMIT;
