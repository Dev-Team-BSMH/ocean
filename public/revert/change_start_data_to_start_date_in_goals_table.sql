-- Revert pacific-ocean:change_start_data_to_start_date_in_goals_table from pg

BEGIN;

ALTER TABLE records.goals
RENAME start_date to start_data;

COMMIT;
